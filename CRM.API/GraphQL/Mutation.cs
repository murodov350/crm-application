﻿using CRM.API.InputTypes;
using CRM.API.Logics;
using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL
{
    public class Mutation
    {
        #region comment ctor
        //private readonly ImainCounterpartyService _counterpartyService;
        //private readonly ImainFactoryService _factoryService;
        //private readonly ImainManagerCounterpartyService _managerCounterpartyService;
        //private readonly ImainManagerService _managerService;
        //private readonly ImainManagerRoleService _managerRoleService;
        //private readonly ImainMoneyService _moneyService;
        //private readonly ImainMoneyCommentService _moneyCommentService;
        //private readonly ImainOrderService _orderService;
        //private readonly ImainOrderCommentService _orderCommentService;
        //private readonly ImainPermissionService _permissionService;
        //private readonly ImainProductService _productService;
        //private readonly ImainPurchaseService _purchaseService;
        //private readonly ImainReturnPurchaseService _returnPurchaseService;
        //private readonly ImainReturnSaleService _returnSaleService;
        //private readonly ImainRolesPermissionsService _rolesPermissionsService;
        //private readonly ImainRolsService _rolsService;
        //private readonly ImainSaleService _saleService;
        //private readonly ImainSectionService _sectionService;
        //private readonly ImainShowcaseService _showcaseService;
        //private readonly ImainUsersService _usersService;
        //private readonly ImainWarehouseService _warehouseService;
        //private readonly ImoneyPaymentTypeService _paymentTypeService;
        //private readonly ImoneyPurposePymentService _moneyPurposePymentService;
        //private readonly IproductCategoryService _productCategoryService;
        //private readonly IproductUnitService _productUnitService;

        //public Mutation(
        //    [Service] ImainCounterpartyService counterpartyService,
        //   [Service] ImainManagerService managerService,
        //   [Service] ImainManagerCounterpartyService managerCounterpartyService,
        //   [Service] ImainFactoryService factoryService,
        //    [Service] ImainManagerRoleService managerRoleService,
        //    [Service] ImainMoneyService moneyService,
        //   [Service] ImainMoneyCommentService moneyCommentService,
        //   [Service] ImainOrderService orderService,
        //   [Service] ImainOrderCommentService orderCommentService,
        //    [Service] ImainPermissionService permissionService,
        //    [Service] ImainProductService productService,
        //   [Service] ImainPurchaseService purchaseService,
        //   [Service] ImainReturnPurchaseService returnPurchaseService,
        //   [Service] ImainReturnSaleService returnSaleService,
        //   [Service] ImainRolesPermissionsService rolesPermissionsService,
        //   [Service] ImainRolsService rolsService,
        //   [Service] ImainSaleService saleService,
        //   [Service] ImainSectionService sectionService,
        //   [Service] ImainShowcaseService showcaseService,
        //   [Service] ImainUsersService usersService,
        //   [Service] ImainWarehouseService warehouseService,
        //  [Service] ImoneyPaymentTypeService paymentTypeService,
        //   [Service] ImoneyPurposePymentService moneyPurposePymentService,
        //  [Service] IproductCategoryService productCategoryService,
        //   [Service] IproductUnitService productUnitService)
        //{
        //    _managerCounterpartyService = managerCounterpartyService;
        //    _managerService = managerService;
        //    _counterpartyService = counterpartyService;
        //    _factoryService = factoryService;
        //    _managerRoleService = managerRoleService;
        //    _moneyService = moneyService;
        //    _moneyCommentService = moneyCommentService;
        //    _orderService = orderService;
        //    _orderCommentService = orderCommentService;
        //    _permissionService = permissionService;
        //    _productService = productService;
        //    _purchaseService = purchaseService;
        //    _returnPurchaseService = returnPurchaseService;
        //    _returnSaleService = returnSaleService;
        //    _rolesPermissionsService = rolesPermissionsService;
        //    _rolsService = rolsService;
        //    _saleService = saleService;
        //    _sectionService = sectionService;
        //    _showcaseService = showcaseService;
        //    _usersService = usersService;
        //    _warehouseService = warehouseService;
        //    _paymentTypeService = paymentTypeService;
        //    _moneyPurposePymentService = moneyPurposePymentService;
        //    _productCategoryService = productCategoryService;
        //    _productUnitService = productUnitService;
        //}
        #endregion
        public string Register([Service] IAuthLogic authLogic, RegisterInputType registerInput)
        {
            return authLogic.Register(registerInput);
        }
        public string Login([Service] IAuthLogic authLogic, LoginInputType loginInput)
        {
            return authLogic.Login(loginInput);
        }
        ///
        [Authorize(Policy = "CreateCounterparty")]
        public mainCounterparty CreateCounterparty([Service] ImainCounterpartyService _counterpartyService, counterpartyInput counterpartyInput)
        {
            return _counterpartyService.Create(counterpartyInput);
        }
        [Authorize(Policy = "UpdateCounterparty")]
        public mainCounterparty UpdateCounterparty([Service] ImainCounterpartyService _counterpartyService, counterpartyInput counterparty)
        {
            return _counterpartyService.Update(counterparty);
        }
        [Authorize(Policy = "DeleteCounterparty")]
        public mainCounterparty DeleteCounterparty([Service] ImainCounterpartyService _counterpartyService, int? id)
        {
            return _counterpartyService.Delete(id);
        }
        [Authorize(Policy = "DeleteCounterparty")]
        public IEnumerable<Boolean> DeleteSelectionCounterparty([Service] ImainCounterpartyService _counterpartyService, List<int> ids)
        {
            return _counterpartyService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreateFactory")]
        public mainFactory createFactory([Service] ImainFactoryService _factoryService,mainFactory InputFactory)
        {
            return _factoryService.Create(InputFactory);
        }
        [Authorize(Policy = "UpdateFactory")]
        public mainFactory updateFactory([Service] ImainFactoryService _factoryService,factoryInput InputFactory)
        {
            return _factoryService.Update(InputFactory);
        }
        [Authorize(Policy = "DeleteFactory")]
        public mainFactory deleteFactory([Service] ImainFactoryService _factoryService,int? id)
        {
            return _factoryService.Delete(id);
        }
        [Authorize(Policy = "DeleteFactory")]
        public IEnumerable<Boolean> DeleteSelectionFactory([Service] ImainFactoryService _factoryService,List<int> ids)
        {
            return _factoryService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreateManager")]
        public mainManager CreateManager([Service] ImainManagerService _managerService, managerInput input)
        {
            return _managerService.Create(input);
        }
        [Authorize(Policy = "UpdateManager")]
        public mainManager UpdateManager([Service] ImainManagerService _managerService, managerInput model)
        {
            return _managerService.Update(model);
        }
        [Authorize(Policy = "DeleteManager")]
        public mainManager DeleteManager([Service] ImainManagerService _managerService, int? id)
        {
            return _managerService.Delete(id);
        }
        [Authorize(Policy = "DeleteManager")]
        public IEnumerable<Boolean> DeleteSelectionManager([Service] ImainManagerService _managerService, List<int> ids)
        {
            return _managerService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreateManagerRole")]
        public mainManagerRole CreateManagerRole([Service] ImainManagerRoleService _managerRoleService,managerRoleInput input)
        {
            return _managerRoleService.Create(input);
        }
        [Authorize(Policy = "UpdateManagerRole")]
        public mainManagerRole UpdateManagerRole([Service] ImainManagerRoleService _managerRoleService, managerRoleInput model)
        {
            return _managerRoleService.Update(model);
        }
        [Authorize(Policy = "DeleteManagerRole")]
        public mainManagerRole DeleteManagerRole([Service] ImainManagerRoleService _managerRoleService, int? id)
        {
            return _managerRoleService.Delete(id);
        }
        [Authorize(Policy = "DeleteManagerRole")]
        public IEnumerable<Boolean> DeleteSelectionManagerRole([Service] ImainManagerRoleService _managerRoleService, List<int> ids)
        {
            return _managerRoleService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreateManagerCounterparty")]
        public mainManagerCounterparty CreateManagerCounterparty([Service]ImainManagerCounterpartyService _managerCounterpartyService,managerCounterpartyInput input)
        {
            return _managerCounterpartyService.Create(input);
        }
        [Authorize(Policy = "UpdateManagerCounterparty")]
        public mainManagerCounterparty UpdateManagerCounterparty([Service] ImainManagerCounterpartyService _managerCounterpartyService, managerCounterpartyInput model)
        {
            return _managerCounterpartyService.Update(model);
        }
        [Authorize(Policy = "DeleteManagerCounterparty")]
        public mainManagerCounterparty DeleteManagerCounterparty([Service] ImainManagerCounterpartyService _managerCounterpartyService, int? id)
        {
            return _managerCounterpartyService.Delete(id);
        }
        [Authorize(Policy = "DeleteManagerCounterparty")]
        public IEnumerable<Boolean> DeleteSelectionManagerCounterparty([Service] ImainManagerCounterpartyService _managerCounterpartyService, List<int> ids)
        {
            return _managerCounterpartyService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreateMoney")]
        public mainMoney CreateMoney([Service] ImainMoneyService _moneyService,moneyInput input)
        {
            return _moneyService.Create(input);
        }
        [Authorize(Policy = "UpdateMoney")]
        public mainMoney UpdateMoney([Service] ImainMoneyService _moneyService, moneyInput model)
        {
            return _moneyService.Update(model);
        }
        [Authorize(Policy = "DeleteMoney")]
        public mainMoney DeleteMoney([Service] ImainMoneyService _moneyService, int? id)
        {
            return _moneyService.Delete(id);
        }
        [Authorize(Policy = "DeleteMoney")]
        public IEnumerable<Boolean> DeleteSelectionMoney([Service] ImainMoneyService _moneyService, List<int> ids)
        {
            return _moneyService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreateOrder")]
        public mainOrder CreateOrder([Service] ImainOrderService _orderService,orderInput input)
        {
            return _orderService.Create(input);
        }
        [Authorize(Policy = "UpdateOrder")]
        public mainOrder UpdateOrder([Service] ImainOrderService _orderService, orderInput model)
        {
            return _orderService.Update(model);
        }
        [Authorize(Policy = "DeleteOrder")]
        public mainOrder DeleteOrder([Service] ImainOrderService _orderService, int? id)
        {
            return _orderService.Delete(id);
        }
        [Authorize(Policy = "DeleteOrder")]
        public IEnumerable<Boolean> DeleteSelectionOrder([Service] ImainOrderService _orderService, List<int> ids)
        {
            return _orderService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreateOrderComment")]
        public mainOrderComment CreateOrderComment([Service] ImainOrderCommentService _orderCommentService,orderCommentInput input)
        {
            return _orderCommentService.Create(input);
        }
        [Authorize(Policy = "UpdateOrderComment")]
        public mainOrderComment UpdateOrderComment([Service] ImainOrderCommentService _orderCommentService, orderCommentInput model)
        {
            return _orderCommentService.Update(model);
        }
        [Authorize(Policy = "DeleteOrderComment")]
        public mainOrderComment DeleteOrderComment([Service] ImainOrderCommentService _orderCommentService, int? id)
        {
            return _orderCommentService.Delete(id);
        }
        [Authorize(Policy = "DeleteOrderComment")]
        public IEnumerable<Boolean> DeleteSelectionOrderComment([Service] ImainOrderCommentService _orderCommentService, List<int> ids)
        {
            return _orderCommentService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreatePermission")]
        public mainPermission CreatePermission([Service] ImainPermissionService _permissionService,permissionInput input)
        {
            return _permissionService.Create(input);
        }
        [Authorize(Policy = "UpdatePermission")]
        public mainPermission UpdatePermission([Service] ImainPermissionService _permissionService, permissionInput model)
        {
            return _permissionService.Update(model);
        }
        [Authorize(Policy = "DeletePermission")]
        public mainPermission DeletePermission([Service] ImainPermissionService _permissionService, int? id)
        {
            return _permissionService.Delete(id);
        }
        [Authorize(Policy = "DeletePermission")]
        public IEnumerable<Boolean> DeleteSelectionPermission([Service] ImainPermissionService _permissionService, List<int> ids)
        {
            return _permissionService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreateProduct")]
        public mainProduct CreateProduct([Service] ImainProductService _productService,productInput input)
        {
            return _productService.Create(input);
        }
        [Authorize(Policy = "UpdateProduct")]
        public mainProduct UpdateProduct([Service] ImainProductService _productService, productInput model)
        {
            return _productService.Update(model);
        }
        [Authorize(Policy = "DeleteProduct")]
        public mainProduct DeleteProduct([Service] ImainProductService _productService, int? id)
        {
            return _productService.Delete(id);
        }
        [Authorize(Policy = "DeleteProduct")]
        public IEnumerable<Boolean> DeleteSelectionProduct([Service] ImainProductService _productService, List<int> ids)
        {
            return _productService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreatePurchase")]
        public mainPurchase CreatePurchase([Service] ImainPurchaseService _purchaseService,purchaseInput input)
        {
            return _purchaseService.Create(input);
        }
        [Authorize(Policy = "UpdatePurchase")]
        public mainPurchase UpdatePurchase([Service] ImainPurchaseService _purchaseService, purchaseInput model)
        {
            return _purchaseService.Update(model);
        }
        [Authorize(Policy = "DeletePurchase")]
        public mainPurchase DeletePurchase([Service] ImainPurchaseService _purchaseService, int? id)
        {
            return _purchaseService.Delete(id);
        }
        [Authorize(Policy = "DeletePurchase")]
        public IEnumerable<Boolean> DeleteSelectionPurchase([Service] ImainPurchaseService _purchaseService, List<int> ids)
        {
            return _purchaseService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreateReturnPurchase")]
        public mainReturnPurchase CreateReturnPurchase([Service] ImainReturnPurchaseService _returnPurchaseService,returnPurchaseInput input)
        {
            return _returnPurchaseService.Create(input);
        }
        [Authorize(Policy = "UpdateReturnPurchase")]
        public mainReturnPurchase UpdateReturnPurchase([Service] ImainReturnPurchaseService _returnPurchaseService, returnPurchaseInput model)
        {
            return _returnPurchaseService.Update(model);
        }
        [Authorize(Policy = "DeleteReturnPurchase")]
        public mainReturnPurchase DeleteReturnPurchase([Service] ImainReturnPurchaseService _returnPurchaseService, int? id)
        {
            return _returnPurchaseService.Delete(id);
        }
        [Authorize(Policy = "DeleteReturnPurchase")]
        public IEnumerable<Boolean> DeleteSelectionReturnPurchase([Service] ImainReturnPurchaseService _returnPurchaseService, List<int> ids)
        {
            return _returnPurchaseService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreateSale")]
        public mainSale CreateSale([Service] ImainSaleService _saleService,saleInput input)
        {
            return _saleService.Create(input);
        }
        [Authorize(Policy = "UpdateSale")]
        public mainSale UpdateSale([Service] ImainSaleService _saleService, saleInput model)
        {
            return _saleService.Update(model);
        }
        [Authorize(Policy = "DeleteSale")]
        public mainSale DeleteSale([Service] ImainSaleService _saleService, int? id)
        {
            return _saleService.Delete(id);
        }
        [Authorize(Policy = "DeleteSale")]
        public IEnumerable<Boolean> DeleteSelectionSale([Service] ImainSaleService _saleService, List<int> ids)
        {
            return _saleService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreateReturnSale")]
        public mainReturnSale CreateReturnSale([Service] ImainReturnSaleService _returnSaleService,returnSaleInput input)
        {
            return _returnSaleService.Create(input);
        }
        [Authorize(Policy = "UpdateReturnSale")]
        public mainReturnSale UpdateReturnSale([Service] ImainReturnSaleService _returnSaleService, returnSaleInput model)
        {
            return _returnSaleService.Update(model);
        }
        [Authorize(Policy = "DeleteReturnSale")]
        public mainReturnSale DeleteReturnSale([Service] ImainReturnSaleService _returnSaleService, int? id)
        {
            return _returnSaleService.Delete(id);
        }
        [Authorize(Policy = "DeleteReturnSale")]
        public IEnumerable<Boolean> DeleteSelectionReturnSale([Service] ImainReturnSaleService _returnSaleService, List<int> ids)
        {
            return _returnSaleService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreateRolesPermissions")]
        public mainRolesPermissions CreateRolesPermissions([Service]ImainRolesPermissionsService _rolesPermissionsService,rolesPermissionsInput input)
        {
            return _rolesPermissionsService.Create(input);
        }
        [Authorize(Policy = "UpdateRolesPermissions")]
        public mainRolesPermissions UpdateRolesPermissions([Service] ImainRolesPermissionsService _rolesPermissionsService, rolesPermissionsInput model)
        {
            return _rolesPermissionsService.Update(model);
        }
        [Authorize(Policy = "DeleteRolesPermissions")]
        public mainRolesPermissions DeleteRolesPermissions([Service] ImainRolesPermissionsService _rolesPermissionsService, int? id)
        {
            return _rolesPermissionsService.Delete(id);
        }
        [Authorize(Policy = "DeleteRolesPermissions")]
        public IEnumerable<Boolean> DeleteSelectionRolesPermissions([Service] ImainRolesPermissionsService _rolesPermissionsService, List<int> ids)
        {
            return _rolesPermissionsService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreateRols")]
        public mainRols CreateRols([Service] ImainRolsService _rolsService,rolsInput input)
        {
            return _rolsService.Create(input);
        }
        [Authorize(Policy = "UpdateRols")]
        public mainRols UpdateRols([Service] ImainRolsService _rolsService, rolsInput model)
        {
            return _rolsService.Update(model);
        }
        [Authorize(Policy = "DeleteRols")]
        public mainRols DeleteRols([Service] ImainRolsService _rolsService, int? id)
        {
            return _rolsService.Delete(id);
        }
        [Authorize(Policy = "DeleteRols")]
        public IEnumerable<Boolean> DeleteSelectionRols([Service] ImainRolsService _rolsService, List<int> ids)
        {
            return _rolsService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreateSection")]
        public mainSection CreateSection([Service] ImainSectionService _sectionService,sectionInput input)
        {
            return _sectionService.Create(input);
        }
        [Authorize(Policy = "UpdateSection")]
        public mainSection UpdateSection([Service] ImainSectionService _sectionService, sectionInput model)
        {
            return _sectionService.Update(model);
        }
        [Authorize(Policy = "DeleteSection")]
        public mainSection DeleteSection([Service] ImainSectionService _sectionService, int? id)
        {
            return _sectionService.Delete(id);
        }
        [Authorize(Policy = "DeleteSection")]
        public IEnumerable<Boolean> DeleteSelectionSection([Service] ImainSectionService _sectionService, List<int> ids)
        {
            return _sectionService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreateShowcase")]
        public mainShowcase CreateShowcase([Service] ImainShowcaseService _showcaseService,showcaseInput input)
        {
            return _showcaseService.Create(input);
        }
        [Authorize(Policy = "UpdateShowcase")]
        public mainShowcase UpdateShowcase([Service] ImainShowcaseService _showcaseService, showcaseInput model)
        {
            return _showcaseService.Update(model);
        }
        [Authorize(Policy = "DeleteShowcase")]
        public mainShowcase DeleteShowcase([Service] ImainShowcaseService _showcaseService, int? id)
        {
            return _showcaseService.Delete(id);
        }
        [Authorize(Policy = "DeleteShowcase")]
        public IEnumerable<Boolean> DeleteSelectionShowcase([Service] ImainShowcaseService _showcaseService, List<int> ids)
        {
            return _showcaseService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreateUsers")]
        public mainUsers CreateUsers([Service]ImainUsersService _usersService,usersInput input)
        {
            return _usersService.Create(input);
        }
        [Authorize(Policy = "UpdateUsers")]
        public mainUsers UpdateUsers([Service] ImainUsersService _usersService,usersInput model)
        {
            return _usersService.Update(model);
        }
        [Authorize(Policy = "DeleteUsers")]
        public mainUsers DeleteUsers([Service] ImainUsersService _usersService,int? id)
        {
            return _usersService.Delete(id);
        }
        [Authorize(Policy = "DeleteUsers")]
        public IEnumerable<Boolean> DeleteSelectionUsers([Service] ImainUsersService _usersService,List<int> ids)
        {
            return _usersService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreateWarehouse")]
        public mainWarehouse CreateWarehouse([Service]ImainWarehouseService _warehouseService,warehouseInput input)
        {
            return _warehouseService.Create(input);
        }
        [Authorize(Policy = "UpdateWarehouse")]
        public mainWarehouse UpdateWarehouse([Service] ImainWarehouseService _warehouseService, warehouseInput model)
        {
            return _warehouseService.Update(model);
        }
        [Authorize(Policy = "DeleteWarehouse")]
        public mainWarehouse DeleteWarehouse([Service] ImainWarehouseService _warehouseService, int? id)
        {
            return _warehouseService.Delete(id);
        }
        [Authorize(Policy = "DeleteWarehouse")]
        public IEnumerable<Boolean> DeleteSelectionWarehouse([Service] ImainWarehouseService _warehouseService, List<int> ids)
        {
            return _warehouseService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreateMoneyComment")]
        public mainMoneyComment CreateMoneyComment([Service] ImainMoneyCommentService _moneyCommentService,moneyCommentInput input)
        {
            return _moneyCommentService.Create(input);
        }
        [Authorize(Policy = "UpdateMoneyComment")]
        public mainMoneyComment UpdateMoneyComment([Service] ImainMoneyCommentService _moneyCommentService, moneyCommentInput model)
        {
            return _moneyCommentService.Update(model);
        }
        [Authorize(Policy = "DeleteMoneyComment")]
        public mainMoneyComment DeleteMoneyComment([Service] ImainMoneyCommentService _moneyCommentService, int? id)
        {
            return _moneyCommentService.Delete(id);
        }
        [Authorize(Policy = "DeleteMoneyComment")]
        public IEnumerable<Boolean> DeleteSelectionMoneyComment([Service] ImainMoneyCommentService _moneyCommentService, List<int> ids)
        {
            return _moneyCommentService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreateMoneyPaymentType")]
        public moneyPaymentType CreateMoneyPaymentType([Service] ImoneyPaymentTypeService _paymentTypeService,moneyPaymentTypeInputs input)
        {
            return _paymentTypeService.Create(input);
        }
        [Authorize(Policy = "UpdateMoneyPaymentType")]
        public moneyPaymentType UpdateMoneyPaymentType([Service] ImoneyPaymentTypeService _paymentTypeService, moneyPaymentTypeInputs model)
        {
            return _paymentTypeService.Update(model);
        }
        [Authorize(Policy = "DeleteMoneyPaymentType")]
        public moneyPaymentType DeleteMoneyPaymentType([Service] ImoneyPaymentTypeService _paymentTypeService, int? id)
        {
            return _paymentTypeService.Delete(id);
        }
        [Authorize(Policy = "DeleteMoneyPaymentType")]
        public IEnumerable<Boolean> DeleteSelectionMoneyPaymentType([Service] ImoneyPaymentTypeService _paymentTypeService, List<int> ids)
        {
            return _paymentTypeService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreateMoneyPurposePyment")]
        public moneyPurposePyment CreateMoneyPurposePyment([Service] ImoneyPurposePymentService _moneyPurposePymentService,moneyPurposePymentInputs input)
        {
            return _moneyPurposePymentService.Create(input);
        }
        [Authorize(Policy = "UpdateMoneyPurposePyment")]
        public moneyPurposePyment UpdateMoneyPurposePyment([Service] ImoneyPurposePymentService _moneyPurposePymentService,moneyPurposePymentInputs model)
        {
            return _moneyPurposePymentService.Update(model);
        }
        [Authorize(Policy = "DeleteMoneyPurposePyment")]
        public moneyPurposePyment DeleteMoneyPurposePyment([Service] ImoneyPurposePymentService _moneyPurposePymentService, int? id)
        {
            return _moneyPurposePymentService.Delete(id);
        }
        [Authorize(Policy = "DeleteMoneyPurposePyment")]
        public IEnumerable<Boolean> DeleteSelectionMoneyPurposePyment([Service] ImoneyPurposePymentService _moneyPurposePymentService, List<int> ids)
        {
            return _moneyPurposePymentService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreateProductCategory")]
        public productCategory CreateProductCategory([Service] IproductCategoryService _productCategoryService,productCategoryInputs input)
        {
            return _productCategoryService.Create(input);
        }
        [Authorize(Policy = "UpdateProductCategory")]
        public productCategory UpdateProductCategory([Service] IproductCategoryService _productCategoryService, productCategoryInputs model)
        {
            return _productCategoryService.Update(model);
        }
        [Authorize(Policy = "DeleteProductCategory")]
        public productCategory DeleteProductCategory([Service] IproductCategoryService _productCategoryService, int? id)
        {
            return _productCategoryService.Delete(id);
        }
        [Authorize(Policy = "DeleteProductCategory")]
        public IEnumerable<Boolean> DeleteSelectionProductCategory([Service] IproductCategoryService _productCategoryService, List<int> ids)
        {
            return _productCategoryService.DeleteSelection(ids);
        }
        ///
        ///
        [Authorize(Policy = "CreateProductUnit")]
        public productUnit CreateProductUnit([Service] IproductUnitService _productUnitService,productUnitInputs input)
        {
            return _productUnitService.Create(input);
        }
        [Authorize(Policy = "UpdateProductUnit")]
        public productUnit UpdateProductUnit([Service] IproductUnitService _productUnitService, productUnitInputs model)
        {
            return _productUnitService.Update(model);
        }
        [Authorize(Policy = "DeleteProductUnit")]
        public productUnit DeleteProductUnit([Service] IproductUnitService _productUnitService, int? id)
        {
            return _productUnitService.Delete(id);
        }
        [Authorize(Policy = "DeleteProductUnit")]
        public IEnumerable<Boolean> DeleteSelectionProductUnit([Service] IproductUnitService _productUnitService, List<int> ids)
        {
            return _productUnitService.DeleteSelection(ids);
        }
        ///
    }
}

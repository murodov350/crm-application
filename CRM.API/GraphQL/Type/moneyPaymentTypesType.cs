﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class moneyPaymentTypesType : ObjectType<moneyPaymentType>
    {
        protected override void Configure(IObjectTypeDescriptor<moneyPaymentType> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field(x => x.Name).Type<StringType>();
            descriptor.Field<moneyPaymentTypeResolver>(x => x.GetMoney(default, default));
        }
        public class moneyPaymentTypeResolver
        {
            private readonly ImainMoneyService _moneyService;
            public moneyPaymentTypeResolver([Service] ImainMoneyService moneyService)
            {
                _moneyService = moneyService;
            }
            public IEnumerable<mainMoney> GetMoney(moneyPaymentType moneyPayment, IResolverContext ctx)
            {
                return _moneyService.GetAll().Where(p => p.PaymentTypeId == moneyPayment.Id);
            }
        }
    }
}

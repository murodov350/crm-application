﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class mainManagerCounterpartyType : ObjectType<mainManagerCounterparty>
    {
        protected override void Configure(IObjectTypeDescriptor<mainManagerCounterparty> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field<mainCounterpartyManagerResolver>(x => x.GetCounterparty(default, default));
            descriptor.Field<mainCounterpartyManagerResolver>(x => x.GetManager(default, default));
        }
    }
    public class mainCounterpartyManagerResolver
    {
        private readonly ImainCounterpartyService _counterpartyService;
        private readonly ImainManagerService _managerService;
        public mainCounterpartyManagerResolver([Service] ImainCounterpartyService counterpartyService, [Service] ImainManagerService managerService)
        {
            _counterpartyService = counterpartyService;
            _managerService = managerService;
        }
        public mainCounterparty GetCounterparty(mainManagerCounterparty managerCounterparty, IResolverContext ctx)
        {
            return _counterpartyService.GetById(managerCounterparty.CounterpartyId);
        }

        public mainManager GetManager(mainManagerCounterparty managerCounterparty, IResolverContext ctx)
        {
            return _managerService.GetById(managerCounterparty.ManagerId);
        }


    }
}
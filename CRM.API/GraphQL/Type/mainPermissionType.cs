﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class mainPermissionType : ObjectType<mainPermission>
    {
        protected override void Configure(IObjectTypeDescriptor<mainPermission> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field(x => x.ResourceName).Type<StringType>();
            descriptor.Field(x => x.Name).Type<StringType>();
            descriptor.Field(x => x.Comment).Type<StringType>();
            descriptor.Field<mainRolesPermissionsResolver>(x => x.GetRolesPermissions(default, default));
        }
        public class mainRolesPermissionsResolver
        {
            private readonly ImainRolesPermissionsService _mainRolesPermissionsService;
            public mainRolesPermissionsResolver([Service] ImainRolesPermissionsService mainRolesPermissionsService)
            {
                _mainRolesPermissionsService = mainRolesPermissionsService;
            }
            public IEnumerable<mainRolesPermissions> GetRolesPermissions(mainPermission permission, IResolverContext ctx)
            {
                return _mainRolesPermissionsService.GetAll().Where(p => p.PermissionId == permission.Id);
            }
        }
    }
}

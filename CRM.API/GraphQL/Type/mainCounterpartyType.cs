﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class mainCounterpartyType : ObjectType<mainCounterparty>
    {
        protected override void Configure(IObjectTypeDescriptor<mainCounterparty> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field(x => x.Lastname).Type<NonNullType<StringType>>();
            descriptor.Field(x => x.Name).Type<StringType>();
            descriptor.Field(x => x.Middlename).Type<StringType>();
            descriptor.Field(x => x.Phone).Type<StringType>();
            descriptor.Field(x => x.Address).Type<StringType>();
            descriptor.Field<mainManagerCounterpartyResolver>(x => x.GetManagerCounterparty(default, default));
            descriptor.Field<mainMoneyResolver>(x => x.GetMoney(default, default));
            descriptor.Field<mainMoneyCommentResolver>(x => x.GetMoneyComment(default, default));
            descriptor.Field<mainPurchaseResolver>(x => x.GetPurchase(default, default));
            descriptor.Field<mainSaleResolver>(x => x.GetSale(default, default));
            descriptor.Field<mainShowcaseResolver>(x => x.GetShowcase(default, default));
            descriptor.Field<mainUsersResolver>(x => x.GetUsers(default, default));
            descriptor.Field<mainCounterpartyTypeResolver>(x => x.GetOrders(default, default));
        }

        public class mainCounterpartyTypeResolver
        {
            private readonly ImainOrderService _orderService;
            public mainCounterpartyTypeResolver([Service] ImainOrderService orderService)
            {
                _orderService = orderService;
            }
            public IEnumerable<mainOrder> GetOrders(mainCounterparty counterparty, IResolverContext ctx)
            {
                return _orderService.GetAll().Where(p => p.CounterpartyId == counterparty.Id);
            }
        }


        public class mainUsersResolver
        {
            private readonly ImainUsersService _mainUsersService;
            public mainUsersResolver([Service] ImainUsersService mainUsersService)
            {
                _mainUsersService = mainUsersService;
            }
            public IEnumerable<mainUsers> GetUsers(mainCounterparty counterparty, IResolverContext ctx)
            {
                return _mainUsersService.GetAll().Where(p => p.CounterpartyId == counterparty.Id);
            }
        }
        public class mainShowcaseResolver
        {
            private readonly ImainShowcaseService _mainShowcaseService;
            public mainShowcaseResolver([Service] ImainShowcaseService mainShowcaseService)
            {
                _mainShowcaseService = mainShowcaseService;
            }
            public IEnumerable<mainShowcase> GetShowcase(mainCounterparty counterparty, IResolverContext ctx)
            {
                return _mainShowcaseService.GetAll().Where(p => p.CounterpartyId == counterparty.Id);
            }
        }
        public class mainSaleResolver
        {
            private readonly ImainSaleService _mainSaleService;
            public mainSaleResolver([Service] ImainSaleService mainSaleService)
            {
                _mainSaleService = mainSaleService;
            }
            public IEnumerable<mainSale> GetSale(mainCounterparty counterparty, IResolverContext ctx)
            {
                return _mainSaleService.GetAll().Where(p => p.CounterpartyId == counterparty.Id);
            }
        }
        public class mainPurchaseResolver
        {
            private readonly ImainPurchaseService _mainPurchaseService;
            public mainPurchaseResolver([Service] ImainPurchaseService mainPurchaseService)
            {
                _mainPurchaseService = mainPurchaseService;
            }
            public IEnumerable<mainPurchase> GetPurchase(mainCounterparty counterparty, IResolverContext ctx)
            {
                return _mainPurchaseService.GetAll().Where(p => p.CounterpartyId == counterparty.Id);
            }
        }
        public class mainMoneyCommentResolver
        {
            private readonly ImainMoneyCommentService _mainMoneyCommentService;
            public mainMoneyCommentResolver([Service] ImainMoneyCommentService mainMoneyCommentService)
            {
                _mainMoneyCommentService = mainMoneyCommentService;
            }
            public IEnumerable<mainMoneyComment> GetMoneyComment(mainCounterparty counterparty, IResolverContext ctx)
            {
                return _mainMoneyCommentService.GetAll().Where(p => p.CounterpartyId == counterparty.Id);
            }
        }
        public class mainMoneyResolver
        {
            private readonly ImainMoneyService _mainMoneyService;
            public mainMoneyResolver([Service] ImainMoneyService mainMoneyService)
            {
                _mainMoneyService = mainMoneyService;
            }
            public IEnumerable<mainMoney> GetMoney(mainCounterparty counterparty, IResolverContext ctx)
            {
                return _mainMoneyService.GetAll().Where(p => p.CounterpartyId == counterparty.Id);
            }
        }
        public class mainManagerCounterpartyResolver
        {
            private readonly ImainManagerCounterpartyService _managerCounterpartyService;
            public mainManagerCounterpartyResolver([Service] ImainManagerCounterpartyService managerCounterpartyService)
            {
                _managerCounterpartyService = managerCounterpartyService;
            }
            public IEnumerable<mainManagerCounterparty> GetManagerCounterparty(mainCounterparty counterparty, IResolverContext ctx)
            {
                return _managerCounterpartyService.GetAll().Where(p => p.CounterpartyId == counterparty.Id);
            }
        }
    }
}

﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class mainRolsType : ObjectType<mainRols>
    {
        protected override void Configure(IObjectTypeDescriptor<mainRols> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field(x => x.ParentId).Type<IntType>();
            descriptor.Field(x => x.ResourceName).Type<StringType>();
            descriptor.Field(x => x.Status).Type<BooleanType>();
            descriptor.Field<mainRolsResolver>(x => x.GetRolesPermissions(default, default));
            descriptor.Field<mainRolsResolver>(x => x.GetUsers(default, default));
        }
        public class mainRolsResolver
        {
            private readonly ImainRolesPermissionsService _rolesPermissionsService;
            private readonly ImainUsersService _usersService;
            public mainRolsResolver([Service] ImainRolesPermissionsService rolesPermissionsService, 
                [Service]ImainUsersService usersService)
            {
                _rolesPermissionsService = rolesPermissionsService;
                _usersService = usersService;
            }
            public IEnumerable<mainRolesPermissions> GetRolesPermissions(mainRols rols, IResolverContext ctx)
            {
                return _rolesPermissionsService.GetAll().Where(p => p.RolsId == rols.Id);
            } 
            public IEnumerable<mainUsers> GetUsers(mainRols rols, IResolverContext ctx)
            {
                return _usersService.GetAll().Where(p => p.Rol == rols.Id);
            }
        }
    }
}

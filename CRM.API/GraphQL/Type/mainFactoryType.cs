﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class mainFactoryType : ObjectType<mainFactory>
    {
        protected override void Configure(IObjectTypeDescriptor<mainFactory> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field(x => x.Name).Type<StringType>();
            descriptor.Field(x => x.Phone).Type<StringType>();
            descriptor.Field(x => x.Email).Type<StringType>();
            descriptor.Field(x => x.Address).Type<StringType>();
            descriptor.Field<mainPurchaseResolver>(x => x.GetPurchases(default, default));
            descriptor.Field<mainReturnPurchaseResolver>(x => x.GetReturnPurchases(default, default));
            descriptor.Field<mainReturnSaleResolver>(x => x.GetReturnSales(default, default));
        }
        public class mainReturnSaleResolver
        {
            private readonly ImainReturnSaleService _mainReturnSaleService;
            public mainReturnSaleResolver([Service] ImainReturnSaleService mainReturnSaleService)
            {
                _mainReturnSaleService = mainReturnSaleService;
            }
            public IEnumerable<mainReturnSale> GetReturnSales(mainFactory factory, IResolverContext ctx)
            {
                return _mainReturnSaleService.GetAll().Where(p => p.FactoryId == factory.Id);
            }
        }
        public class mainReturnPurchaseResolver
        {
            private readonly ImainReturnPurchaseService _returnPurchaseService;
            public mainReturnPurchaseResolver([Service] ImainReturnPurchaseService returnPurchaseService)
            {
                _returnPurchaseService = returnPurchaseService;
            }
            public IEnumerable<mainReturnPurchase> GetReturnPurchases(mainFactory factory, IResolverContext ctx)
            {
                return _returnPurchaseService.GetAll().Where(p => p.FactoryId == factory.Id);
            }
        }
        public class mainPurchaseResolver
        {
            private readonly ImainPurchaseService _purchaseService;
            public mainPurchaseResolver([Service] ImainPurchaseService purchaseService)
            {
                _purchaseService = purchaseService;
            }
            public IEnumerable<mainPurchase> GetPurchases(mainFactory factory, IResolverContext ctx)
            {
                return _purchaseService.GetAll().Where(p => p.FactoryId == factory.Id);
            }
        }
    }
}

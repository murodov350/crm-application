﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class mainSectionType : ObjectType<mainSection>
    {
        protected override void Configure(IObjectTypeDescriptor<mainSection> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field(x => x.Name).Type<StringType>();
            descriptor.Field<mainSectionResolver>(x => x.GetManagerRoles(default, default));
        }
        public class mainSectionResolver
        {
            private readonly ImainManagerRoleService _mainManagerRoleService;
            public mainSectionResolver([Service] ImainManagerRoleService mainManagerRoleService)
            {
                _mainManagerRoleService = mainManagerRoleService;
            }
            public IEnumerable<mainManagerRole> GetManagerRoles(mainSection section, IResolverContext ctx)
            {
                return _mainManagerRoleService.GetAll().Where(p => p.SectionId == section.Id);
            }
        }
    }
}

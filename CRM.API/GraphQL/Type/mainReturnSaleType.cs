﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class mainReturnSaleType : ObjectType<mainReturnSale>
    {
        protected override void Configure(IObjectTypeDescriptor<mainReturnSale> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field(x => x.DateReturnSale).Type<DateTimeType>();
            descriptor.Field(x => x.Document).Type<StringType>();
            descriptor.Field(x => x.Amount).Type<IntType>();
            descriptor.Field(x => x.PriceUZS).Type<FloatType>();
            descriptor.Field(x => x.PriceUSD).Type<FloatType>();
            descriptor.Field<mainReturnSaleResolver>(x => x.GetUser(default, default));
            descriptor.Field<mainReturnSaleResolver>(x => x.GetWarehouse(default, default));
            descriptor.Field<mainReturnSaleResolver>(x => x.GetFactory(default, default));
        }
        public class mainReturnSaleResolver
        {
            private readonly ImainUsersService _usersService;
            private readonly ImainWarehouseService _warehouseService;
            private readonly ImainFactoryService _factoryService;

            public mainReturnSaleResolver(
                [Service] ImainUsersService usersService,
                [Service] ImainWarehouseService warehouseService,
                [Service] ImainFactoryService factoryService)
            {
                _usersService = usersService;
                _warehouseService = warehouseService;
                _factoryService = factoryService;
            }
            public mainUsers GetUser(mainReturnSale returnSale, IResolverContext ctx)
            {
                return _usersService.GetById(returnSale.UsersId);
            }
            public mainWarehouse GetWarehouse(mainReturnSale returnSale, IResolverContext ctx)
            {
                return _warehouseService.GetById(returnSale.WarehouseId);
            }
            public mainFactory GetFactory(mainReturnSale returnSale, IResolverContext ctx)
            {
                return _factoryService.GetById(returnSale.FactoryId);
            }
        }
    }
}

﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class mainReturnPurchaseType : ObjectType<mainReturnPurchase>
    {
        protected override void Configure(IObjectTypeDescriptor<mainReturnPurchase> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field(x => x.DateReturnPurchase).Type<DateTimeType>();
            descriptor.Field(x => x.Document).Type<StringType>();
            descriptor.Field(x => x.Amount).Type<IntType>();
            descriptor.Field(x => x.PriceUSD).Type<FloatType>();
            descriptor.Field<mainReturnPurchaseResolver>(x => x.GetPurchase(default, default));
            descriptor.Field<mainReturnPurchaseResolver>(x => x.GetUser(default, default));
            descriptor.Field<mainReturnPurchaseResolver>(x => x.GetWarehouse(default, default));
            descriptor.Field<mainReturnPurchaseResolver>(x => x.GetFactory(default, default));
        }
        public class mainReturnPurchaseResolver
        {
            private readonly ImainPurchaseService _purchaseService;
            private readonly ImainUsersService _usersService;
            private readonly ImainWarehouseService _warehouseService;
            private readonly ImainFactoryService _factoryService;

            public mainReturnPurchaseResolver(
                [Service] ImainPurchaseService purchaseService,
                [Service] ImainUsersService usersService,
                [Service] ImainWarehouseService warehouseService,
                [Service] ImainFactoryService factoryService)
            {
                _purchaseService = purchaseService;
                _usersService = usersService;
                _warehouseService = warehouseService;
                _factoryService = factoryService;
            }
            public mainPurchase GetPurchase(mainReturnPurchase returnPurchase, IResolverContext ctx)
            {
                return _purchaseService.GetById(returnPurchase.PurchaseId);
            }
            public mainUsers GetUser(mainReturnPurchase returnPurchase, IResolverContext ctx)
            {
                return _usersService.GetById(returnPurchase.UsersId);
            }
            public mainWarehouse GetWarehouse(mainReturnPurchase returnPurchase, IResolverContext ctx)
            {
                return _warehouseService.GetById(returnPurchase.WarehouseId);
            }
            public mainFactory GetFactory(mainReturnPurchase returnPurchase, IResolverContext ctx)
            {
                return _factoryService.GetById(returnPurchase.FactoryId);
            }
        }
    }
}

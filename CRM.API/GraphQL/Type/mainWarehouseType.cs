﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class mainWarehouseType : ObjectType<mainWarehouse>
    {
        protected override void Configure(IObjectTypeDescriptor<mainWarehouse> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field(x => x.Name).Type<StringType>();
            descriptor.Field(x => x.Address).Type<StringType>();
            descriptor.Field<mainWarehouseyResolver>(x => x.GetReturnPurchases(default, default));
            descriptor.Field<mainWarehouseyResolver>(x => x.GetReturnSales(default, default));
            descriptor.Field<mainWarehouseyResolver>(x => x.GetSales(default, default));
            descriptor.Field<mainWarehouseyResolver>(x => x.GetMoneys(default, default));
        }
        public class mainWarehouseyResolver
        {
            private readonly ImainReturnPurchaseService _returnPurchaseService;
            private readonly ImainReturnSaleService _returnSaleService;
            private readonly ImainSaleService _saleService;
            private readonly ImainMoneyService _moneyService;
            public mainWarehouseyResolver([Service] ImainReturnPurchaseService returnPurchaseService,
                [Service] ImainReturnSaleService returnSaleService,
                [Service] ImainSaleService saleService,
                [Service] ImainMoneyService moneyService)
            {
                _returnPurchaseService = returnPurchaseService;
                _returnSaleService = returnSaleService;
                _saleService = saleService;
                _moneyService = moneyService;
            }
            public IEnumerable<mainReturnPurchase> GetReturnPurchases(mainWarehouse warehouse, IResolverContext ctx)
            {
                return _returnPurchaseService.GetAll().Where(p => p.UsersId == warehouse.Id);
            }
            public IEnumerable<mainReturnSale> GetReturnSales(mainWarehouse warehouse, IResolverContext ctx)
            {
                return _returnSaleService.GetAll().Where(p => p.UsersId == warehouse.Id);
            }
            public IEnumerable<mainSale> GetSales(mainWarehouse warehouse, IResolverContext ctx)
            {
                return _saleService.GetAll().Where(p => p.UsersId == warehouse.Id);
            }
            public IEnumerable<mainMoney> GetMoneys(mainWarehouse warehouse, IResolverContext ctx)
            {
                return _moneyService.GetAll().Where(p => p.UserId == warehouse.Id);
            }
        }
    }
}

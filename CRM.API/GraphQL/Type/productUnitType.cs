﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class productUnitType : ObjectType<productUnit>
    {
        protected override void Configure(IObjectTypeDescriptor<productUnit> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field(x => x.Name).Type<StringType>();
            descriptor.Field<productUnitResolver>(x => x.GetproductUnit(default, default));
        }
        public class productUnitResolver
        {
            private readonly ImainProductService _productService;
            public productUnitResolver([Service] ImainProductService productService)
            {
                _productService = productService;
            }
            public IEnumerable<mainProduct> GetproductUnit(productUnit productUnit, IResolverContext ctx)
            {
                return _productService.GetAll().Where(p => p.UnitId == productUnit.Id);
            }
        }
    }
}

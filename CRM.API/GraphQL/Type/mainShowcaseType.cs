﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System.Collections.Generic;
using System.Linq;

namespace CRM.API.GraphQL.Type
{
    public class mainShowcaseType : ObjectType<mainShowcase>
    {
        protected override void Configure(IObjectTypeDescriptor<mainShowcase> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field(x => x.Article).Type<StringType>();
            descriptor.Field(x => x.Count).Type<IntType>();
            descriptor.Field<mainShowcaseResolver>(x => x.GetProduct(default, default));
            descriptor.Field<mainShowcaseResolver>(x => x.GetCounterparty(default, default));
            descriptor.Field<mainShowcaseResolver>(x => x.GetSales(default, default));
        }
        public class mainShowcaseResolver
        {
            private readonly ImainProductService _productService;
            private readonly ImainCounterpartyService _counterpartyService;
            private readonly ImainSaleService _saleService;
            public mainShowcaseResolver([Service] ImainProductService productService, [Service] ImainCounterpartyService counterpartyService,[Service] ImainSaleService saleService)
            {
                _productService = productService;
                _counterpartyService = counterpartyService;
                _saleService = saleService;
            }
            public mainProduct GetProduct(mainShowcase showcase, IResolverContext ctx)
            {
                return _productService.GetById(showcase.ProductId);
            }
            public mainCounterparty GetCounterparty(mainShowcase showcase, IResolverContext ctx)
            {
                return _counterpartyService.GetById(showcase.CounterpartyId);
            }

            public IEnumerable<mainSale> GetSales(mainShowcase showcase, IResolverContext ctx)
            {
                return _saleService.GetAll().Where(p => p.ShowcaseId == showcase.Id);
            }
        }
    }
}

﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class mainMoneyCommentType : ObjectType<mainMoneyComment>
    {
        protected override void Configure(IObjectTypeDescriptor<mainMoneyComment> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field(x => x.CommentsText).Type<StringType>();
            descriptor.Field<mainMoneyCommentResolver>(x => x.GetCounterparty(default, default));
            descriptor.Field<mainMoneyCommentResolver>(x => x.GetMoney(default, default));
        }
        public class mainMoneyCommentResolver
        {
            private readonly ImainCounterpartyService _mainCounterpartyService;
            private readonly ImainMoneyService _mainMoneyService;
            public mainMoneyCommentResolver([Service] ImainCounterpartyService mainCounterpartyService,[Service] ImainMoneyService mainMoneyService)
            {
                _mainCounterpartyService = mainCounterpartyService;
                _mainMoneyService = mainMoneyService;
            }
            public mainCounterparty GetCounterparty(mainMoneyComment moneyComment, IResolverContext ctx)
            {
                return _mainCounterpartyService.GetById(moneyComment.CounterpartyId);
            }
            public mainMoney GetMoney(mainMoneyComment moneyComment, IResolverContext ctx)
            {
                return _mainMoneyService.GetById(moneyComment.CounterpartyId);
            }
        }
    }
}

﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class mainOrderType : ObjectType<mainOrder>
    {
        protected override void Configure(IObjectTypeDescriptor<mainOrder> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field(x => x.DateOrder).Type<DateTimeType>();
            descriptor.Field(x => x.Document).Type<StringType>();
            descriptor.Field(x => x.Ammount).Type<StringType>();
            descriptor.Field<mainOrderResolver>(x => x.GetCounterparty(default, default));
            descriptor.Field<mainOrderResolver>(x => x.GetProduct(default, default));
            descriptor.Field<mainOrderResolver>(x => x.GetOrderComments(default, default));
        }
        public class mainOrderResolver
        {
            private readonly ImainOrderCommentService _mainOrderCommentService;
            private readonly ImainCounterpartyService _counterpartyService;
            private readonly ImainProductService _productService;
            public mainOrderResolver([Service] ImainOrderCommentService mainOrderCommentService,[Service] ImainCounterpartyService counterpartyService,[Service] ImainProductService productService)
            {
                _mainOrderCommentService = mainOrderCommentService;
                _counterpartyService = counterpartyService;
                _productService = productService;
            }
            public mainCounterparty GetCounterparty(mainOrder order, IResolverContext ctx)
            {
                return _counterpartyService.GetById(order.CounterpartyId);
            }
            public mainProduct GetProduct(mainOrder order, IResolverContext ctx)
            {
                return _productService.GetById(order.ProductId);
            }
            public IEnumerable<mainOrderComment> GetOrderComments(mainOrder order, IResolverContext ctx)
            {
                return _mainOrderCommentService.GetAll().Where(p => p.OrderId == order.Id);
            }
        }
    }
}

﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class mainManagerRoleType : ObjectType<mainManagerRole>
    {
        protected override void Configure(IObjectTypeDescriptor<mainManagerRole> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field<mainManagerRoleResolver>(x => x.GetManager(default, default));
            descriptor.Field<mainManagerRoleResolver>(x => x.GetSection(default, default));
        }
        public class mainManagerRoleResolver
        {
            private readonly ImainManagerService _mainManagerService;
            private readonly ImainSectionService _mainSectionService;
            public mainManagerRoleResolver([Service] ImainManagerService mainManagerService,[Service] ImainSectionService mainSectionService)
            {
                _mainManagerService = mainManagerService;
                _mainSectionService = mainSectionService;
            }
            public mainManager GetManager(mainManagerRole model, IResolverContext ctx)
            {
                return _mainManagerService.GetById(model.ManagerId);
            }
            public mainSection GetSection(mainManagerRole model, IResolverContext ctx)
            {
                return _mainSectionService.GetById(model.SectionId);
            }
        }
    }
}

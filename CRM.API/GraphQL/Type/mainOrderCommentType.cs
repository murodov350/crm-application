﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class mainOrderCommentType : ObjectType<mainOrderComment>
    {
        protected override void Configure(IObjectTypeDescriptor<mainOrderComment> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field(x => x.CommentsText).Type<StringType>();
            descriptor.Field<mainOrderCommentResolver>(x => x.GetOrder(default, default));
            descriptor.Field<mainOrderCommentResolver>(x => x.GetManager(default, default));
        }
        public class mainOrderCommentResolver
        {
            private readonly ImainManagerService _managerService;
            private readonly ImainOrderService _orderService;
            public mainOrderCommentResolver([Service] ImainManagerService managerService,[Service] ImainOrderService orderService)
            {
                _managerService = managerService;
                _orderService = orderService;
            }
            public mainManager GetManager(mainOrderComment model, IResolverContext ctx)
            {
                return _managerService.GetById(model.ManagerId);
            }
            public mainOrder GetOrder(mainOrderComment model, IResolverContext ctx)
            {
                return _orderService.GetById(model.OrderId);
            }
        }
    }
}

﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class mainManagerType : ObjectType<mainManager>
    {
        protected override void Configure(IObjectTypeDescriptor<mainManager> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field(x => x.Lastname).Type<StringType>();
            descriptor.Field(x => x.Name).Type<StringType>();
            descriptor.Field(x => x.Middlename).Type<StringType>();
            descriptor.Field(x => x.Phone).Type<StringType>();
            descriptor.Field(x => x.Address).Type<StringType>();
            descriptor.Field<mainManagerCounterpartyResolver>(x => x.GetManagerCounterparty(default, default));
            descriptor.Field<mainManagerRoleResolver>(x => x.GetManagerRole(default, default));
            descriptor.Field<mainOrderCommentResolver>(x => x.GetOrderComment(default, default));
            descriptor.Field<mainUsersResolver>(x => x.GetUsers(default, default));
        }
        public class mainUsersResolver
        {
            private readonly ImainUsersService _mainUsersService;
            public mainUsersResolver(ImainUsersService mainUsersService)
            {
                _mainUsersService = mainUsersService;
            }
            public IEnumerable<mainUsers> GetUsers(mainManager manager, IResolverContext ctx)
            {
                return _mainUsersService.GetAll().Where(p => p.ManagerId == manager.Id);
            }
        }
        public class mainOrderCommentResolver
        {
            private readonly ImainOrderCommentService _mainOrderCommentService;
            public mainOrderCommentResolver(ImainOrderCommentService mainOrderCommentService)
            {
                _mainOrderCommentService = mainOrderCommentService;
            }
            public IEnumerable<mainOrderComment> GetOrderComment(mainManager manager, IResolverContext ctx)
            {
                return _mainOrderCommentService.GetAll().Where(p => p.ManagerId == manager.Id);
            }
        }
        public class mainManagerRoleResolver
        {
            private readonly ImainManagerRoleService _mainManagerRoleService;
            public mainManagerRoleResolver(ImainManagerRoleService mainManagerRoleService)
            {
                _mainManagerRoleService = mainManagerRoleService;
            }
            public IEnumerable<mainManagerRole> GetManagerRole(mainManager manager, IResolverContext ctx)
            {
                return _mainManagerRoleService.GetAll().Where(p => p.ManagerId == manager.Id);
            }
        }
        public class mainManagerCounterpartyResolver
        {
            private readonly ImainManagerCounterpartyService _managerCounterpartyService;
            public mainManagerCounterpartyResolver(ImainManagerCounterpartyService managerCounterpartyService)
            {
                _managerCounterpartyService = managerCounterpartyService;
            }
            public IEnumerable<mainManagerCounterparty> GetManagerCounterparty(mainManager manager, IResolverContext ctx)
            {
                return _managerCounterpartyService.GetAll().Where(p => p.ManagerId == manager.Id);
            }
        }
    }
}

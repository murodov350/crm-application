﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class mainUsersType : ObjectType<mainUsers>
    {
        protected override void Configure(IObjectTypeDescriptor<mainUsers> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field(x => x.Login).Type<StringType>();
            descriptor.Field(x => x.Password).Type<StringType>();
            descriptor.Field(x => x.Rol).Type<IntType>();
            descriptor.Field<mainUsersResolver>(x => x.GetManager(default, default));
            descriptor.Field<mainUsersResolver>(x => x.GetCounterparty(default, default));
            descriptor.Field<mainUsersResolver>(x => x.GetRol(default, default));

            descriptor.Field<mainUsersResolver>(x => x.GetPurchases(default, default));
            descriptor.Field<mainUsersResolver>(x => x.GetReturnPurchases(default, default));
            descriptor.Field<mainUsersResolver>(x => x.GetReturnSales(default, default));
            descriptor.Field<mainUsersResolver>(x => x.GetSales(default, default));
            descriptor.Field<mainUsersResolver>(x => x.GetMoneys(default, default));
        }
        public class mainUsersResolver
        {
            private readonly ImainManagerService _managerService;
            private readonly ImainCounterpartyService _counterpartyService;
            private readonly ImainRolsService _rolsService;

            private readonly ImainPurchaseService _purchaseService;
            private readonly ImainReturnPurchaseService _returnPurchaseService;
            private readonly ImainReturnSaleService _returnSaleService;
            private readonly ImainSaleService _saleService;
            private readonly ImainMoneyService _moneyService;

            public mainUsersResolver(
                [Service] ImainManagerService managerService,
                [Service] ImainCounterpartyService counterpartyService,
                [Service] ImainPurchaseService purchaseService,
                [Service] ImainReturnPurchaseService returnPurchaseService,
                [Service] ImainReturnSaleService returnSaleService,
                [Service] ImainSaleService saleService,
                [Service] ImainMoneyService moneyService, 
                [Service] ImainRolsService rolsService)
            {
                _managerService = managerService;
                _counterpartyService = counterpartyService;
                _purchaseService = purchaseService;
                _returnPurchaseService = returnPurchaseService;
                _returnSaleService = returnSaleService;
                _saleService = saleService;
                _moneyService = moneyService;
                _rolsService = rolsService;
            }
            //
            public mainManager GetManager(mainUsers users, IResolverContext ctx)
            {
                return _managerService.GetById(users.ManagerId);
            }
            public mainCounterparty GetCounterparty(mainUsers users, IResolverContext ctx)
            {
                return _counterpartyService.GetById(users.CounterpartyId);
            }
            public mainRols GetRol(mainUsers users, IResolverContext ctx)
            {
                return _rolsService.GetById(users.Rol);
            }
            //
            public IEnumerable<mainPurchase> GetPurchases(mainUsers users, IResolverContext ctx)
            {
                return _purchaseService.GetAll().Where(p => p.UsersId == users.Id);
            }
            public IEnumerable<mainReturnPurchase> GetReturnPurchases(mainUsers users, IResolverContext ctx)
            {
                return _returnPurchaseService.GetAll().Where(p => p.UsersId == users.Id);
            }
            public IEnumerable<mainReturnSale> GetReturnSales(mainUsers users, IResolverContext ctx)
            {
                return _returnSaleService.GetAll().Where(p => p.UsersId == users.Id);
            }
            public IEnumerable<mainSale> GetSales(mainUsers users, IResolverContext ctx)
            {
                return _saleService.GetAll().Where(p => p.UsersId == users.Id);
            }
            public IEnumerable<mainMoney> GetMoneys(mainUsers users, IResolverContext ctx)
            {
                return _moneyService.GetAll().Where(p => p.UserId == users.Id);
            }
        }
    }
}

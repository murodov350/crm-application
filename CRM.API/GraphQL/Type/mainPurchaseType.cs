﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class mainPurchaseType : ObjectType<mainPurchase>
    {
        protected override void Configure(IObjectTypeDescriptor<mainPurchase> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field(x => x.DatePurchase).Type<DateTimeType>();
            descriptor.Field(x => x.Document).Type<StringType>();
            descriptor.Field(x => x.Amount).Type<IntType>();
            descriptor.Field(x => x.PriceUSD).Type<FloatType>();
            descriptor.Field<mainPurchaseResolver>(x => x.GetProduct(default, default));
            descriptor.Field<mainPurchaseResolver>(x => x.GetUser(default, default));
            descriptor.Field<mainPurchaseResolver>(x => x.GetCounterparty(default, default));
            descriptor.Field<mainPurchaseResolver>(x => x.GetFactory(default, default));

            descriptor.Field<mainPurchaseResolver>(x => x.GetReturnPurchases(default, default));
        }
        public class mainPurchaseResolver
        {
            private readonly ImainProductService _productService;
            private readonly ImainUsersService _usersService;
            private readonly ImainCounterpartyService _counterpartyService;
            private readonly ImainFactoryService _factoryService;

            private readonly ImainReturnPurchaseService _returnPurchaseService;

            public mainPurchaseResolver(
               [Service] ImainProductService productService,
               [Service] ImainUsersService usersService,
               [Service] ImainCounterpartyService counterpartyService,
               [Service] ImainFactoryService factoryService,
               [Service] ImainReturnPurchaseService returnPurchaseService)
            {
                _productService = productService;
                _usersService = usersService;
                _counterpartyService = counterpartyService;
                _factoryService = factoryService;
                _returnPurchaseService = returnPurchaseService;
            }
            public mainProduct GetProduct(mainPurchase purchase, IResolverContext ctx)
            {
                return _productService.GetById(purchase.ProductId);
            }
            public mainUsers GetUser(mainPurchase purchase, IResolverContext ctx)
            {
                return _usersService.GetById(purchase.UsersId);
            }
            public mainCounterparty GetCounterparty(mainPurchase purchase, IResolverContext ctx)
            {
                return _counterpartyService.GetById(purchase.CounterpartyId);
            }
            public mainFactory GetFactory(mainPurchase purchase, IResolverContext ctx)
            {
                return _factoryService.GetById(purchase.FactoryId);
            }
            public IEnumerable<mainReturnPurchase> GetReturnPurchases(mainPurchase purchase, IResolverContext ctx)
            {
                return _returnPurchaseService.GetAll().Where(p => p.PurchaseId == purchase.Id);
            }
        }
    }
}

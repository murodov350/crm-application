﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class mainMoneyType : ObjectType<mainMoney>
    {
        protected override void Configure(IObjectTypeDescriptor<mainMoney> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field(x => x.Type).Type<IntType>();
            descriptor.Field(x => x.DateMoney).Type<DateTimeType>();
            descriptor.Field(x => x.ExchangeRate).Type<FloatType>();
            descriptor.Field(x => x.AmmountUSD).Type<FloatType>();
            descriptor.Field<mainMoneyResolver>(x => x.GetWarehouse(default, default));
            descriptor.Field<mainMoneyResolver>(x => x.GetCounterparty(default, default));
            descriptor.Field<mainMoneyResolver>(x => x.GetmoneyPurposePyment(default, default));
            descriptor.Field<mainMoneyResolver>(x => x.GetmoneyPaymentType(default, default));
            descriptor.Field<mainMoneyResolver>(x => x.GetUsers(default, default));

            descriptor.Field<mainMoneyCommentResolver>(x => x.GetMoneyComments(default, default));

        }
        public class mainMoneyResolver
        {
            private readonly ImainWarehouseService _mainWarehouseService;
            private readonly ImainCounterpartyService _mainCounterpartyService;
            private readonly ImoneyPurposePymentService _moneyPurposePymentService;
            private readonly ImoneyPaymentTypeService _moneyPaymentTypeService;
            private readonly ImainUsersService _mainUsersService;
            public mainMoneyResolver(
                  [Service] ImainWarehouseService mainWarehouseService
                 ,[Service] ImainCounterpartyService mainCounterpartyService
                 ,[Service] ImoneyPurposePymentService moneyPurposePymentService
                 ,[Service] ImoneyPaymentTypeService moneyPaymentTypeService
                 ,[Service] ImainUsersService mainUsersService
                )
            {
                _mainWarehouseService = mainWarehouseService;
                _mainCounterpartyService = mainCounterpartyService;
                _moneyPurposePymentService = moneyPurposePymentService;
                _moneyPaymentTypeService = moneyPaymentTypeService;
                _mainUsersService = mainUsersService;
            }
            public mainWarehouse GetWarehouse(mainMoney model, IResolverContext ctx)
            {
                return _mainWarehouseService.GetById(model.WarehouseId);
            }
            public mainCounterparty GetCounterparty(mainMoney model, IResolverContext ctx)
            {
                return _mainCounterpartyService.GetById(model.CounterpartyId);
            }
            public moneyPurposePyment GetmoneyPurposePyment(mainMoney model, IResolverContext ctx)
            {
                return _moneyPurposePymentService.GetById(model.PurposePymentId);
            }
            public moneyPaymentType GetmoneyPaymentType(mainMoney model, IResolverContext ctx)
            {
                return _moneyPaymentTypeService.GetById(model.CounterpartyId);
            }
            public mainUsers GetUsers(mainMoney model, IResolverContext ctx)
            {
                return _mainUsersService.GetById(model.UserId);
            }
        }
        public class mainMoneyCommentResolver
        {
            ImainMoneyService _moneyService;
            public mainMoneyCommentResolver([Service] ImainMoneyService moneyService)
            {
                _moneyService = moneyService;
            }
            public IEnumerable<mainMoney> GetMoneyComments(mainMoneyComment model, IResolverContext ctx)
            {
                return _moneyService.GetAll().Where(p => p.Id == model.MoneyId);
            }
        }
    }
}

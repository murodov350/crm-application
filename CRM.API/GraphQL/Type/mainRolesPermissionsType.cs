﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class mainRolesPermissionsType : ObjectType<mainRolesPermissions>
    {
        protected override void Configure(IObjectTypeDescriptor<mainRolesPermissions> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field(x => x.View).Type<BooleanType>();
            descriptor.Field(x => x.Create).Type<BooleanType>();
            descriptor.Field(x => x.Edit).Type<BooleanType>();
            descriptor.Field(x => x.Delete).Type<BooleanType>();
            descriptor.Field<mainRolesPermissionsResolver>(x => x.GetRol(default, default));
            descriptor.Field<mainRolesPermissionsResolver>(x => x.GetPermission(default, default));
        }
        public class mainRolesPermissionsResolver
        {
            private readonly ImainRolsService _rolsService;
            private readonly ImainPermissionService _permissionService;

            public mainRolesPermissionsResolver([Service] ImainRolsService rolsService, [Service] ImainPermissionService permissionService)
            {
                _rolsService = rolsService;
                _permissionService = permissionService;
            }

            public mainRols GetRol(mainRolesPermissions model, IResolverContext ctx)
            {
                return _rolsService.GetById(model.RolsId);
            }
            public mainPermission GetPermission(mainRolesPermissions model, IResolverContext ctx)
            {
                return _permissionService.GetById(model.PermissionId);
            }
        }
    }
}

﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class moneyPurposePymentType : ObjectType<moneyPurposePyment>
    {
        protected override void Configure(IObjectTypeDescriptor<moneyPurposePyment> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field(x => x.Name).Type<StringType>();
            descriptor.Field<moneyPurposePymentTypeResolver>(x => x.GetMoney(default, default));
        }
        public class moneyPurposePymentTypeResolver
        {
            private readonly ImainMoneyService _moneyService;
            public moneyPurposePymentTypeResolver([Service] ImainMoneyService moneyService)
            {
                _moneyService = moneyService;
            }
            public IEnumerable<mainMoney> GetMoney(moneyPurposePyment moneyPayment, IResolverContext ctx)
            {
                return _moneyService.GetAll().Where(p => p.PurposePymentId == moneyPayment.Id);
            }
        }
    }
}

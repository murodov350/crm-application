﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class productCategoryType : ObjectType<productCategory>
    {
        protected override void Configure(IObjectTypeDescriptor<productCategory> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field(x => x.Name).Type<StringType>();
            descriptor.Field<productCategoryTypeResolver>(x => x.GetproductCategory(default, default));
        }
        public class productCategoryTypeResolver
        {
            private readonly ImainProductService _productService;
            public productCategoryTypeResolver([Service] ImainProductService productService)
            {
                _productService = productService;
            }
            public IEnumerable<mainProduct> GetproductCategory(productCategory productCategory, IResolverContext ctx)
            {
                return _productService.GetAll().Where(p => p.CategoryId == productCategory.Id);
            }
        }
    }
}

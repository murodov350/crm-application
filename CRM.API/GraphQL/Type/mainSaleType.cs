﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class mainSaleType : ObjectType<mainSale>
    {
        protected override void Configure(IObjectTypeDescriptor<mainSale> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field(x => x.DateSale).Type<DateTimeType>();
            descriptor.Field(x => x.Document).Type<StringType>();
            descriptor.Field(x => x.Amount).Type<IntType>();
            descriptor.Field(x => x.PriceUSD).Type<FloatType>();
            descriptor.Field(x => x.PriceUZS).Type<FloatType>();
            descriptor.Field(x => x.Discount).Type<FloatType>();
            descriptor.Field<mainSaleResolver>(x => x.GetShowcase(default, default));
            descriptor.Field<mainSaleResolver>(x => x.GetWarehouse(default, default));
            descriptor.Field<mainSaleResolver>(x => x.GetUser(default, default));
            descriptor.Field<mainSaleResolver>(x => x.GetCounterparty(default, default));
        }
        public class mainSaleResolver
        {
            private readonly ImainShowcaseService _showcaseService;
            private readonly ImainWarehouseService _warehouseService;
            private readonly ImainUsersService _usersService;
            private readonly ImainCounterpartyService _counterpartyService;

            public mainSaleResolver(
               [Service] ImainShowcaseService showcaseService,
               [Service] ImainUsersService usersService,
               [Service] ImainCounterpartyService counterpartyService,
               [Service] ImainWarehouseService warehouseService
                )
            {
                _warehouseService = warehouseService;
                _usersService = usersService;
                _counterpartyService = counterpartyService;
                _showcaseService = showcaseService;
            }
            public mainShowcase GetShowcase(mainSale model, IResolverContext ctx)
            {
                return _showcaseService.GetById(model.ShowcaseId);
            }
            public mainUsers GetUser(mainSale sale, IResolverContext ctx)
            {
                return _usersService.GetById(sale.UsersId);
            }
            public mainCounterparty GetCounterparty(mainSale sale, IResolverContext ctx)
            {
                return _counterpartyService.GetById(sale.CounterpartyId);
            }
            public mainWarehouse GetWarehouse(mainSale sale, IResolverContext ctx)
            {
                return _warehouseService.GetById(sale.WarehouseId);
            }
        }
    }
}

﻿using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.GraphQL.Type
{
    public class mainProductType : ObjectType<mainProduct>
    {
        protected override void Configure(IObjectTypeDescriptor<mainProduct> descriptor)
        {
            descriptor.Field(x => x.Id).Type<IdType>();
            descriptor.Field(x => x.NameModel).Type<StringType>();
            descriptor.Field(x => x.NameParty).Type<StringType>();
            descriptor.Field(x => x.Article).Type<StringType>();
            descriptor.Field(x => x.PurchaseUSD).Type<FloatType>();
            descriptor.Field(x => x.CostPriceUSD).Type<FloatType>();
            descriptor.Field(x => x.SalePriceUSD).Type<FloatType>();
            descriptor.Field(x => x.Discount).Type<FloatType>();
            descriptor.Field(x => x.Photo).Type<StringType>();
            descriptor.Field<mainProductResolver>(x => x.GetproductCategory(default, default));
            descriptor.Field<mainProductResolver>(x => x.GetproductUnit(default, default));

            descriptor.Field<mainProductResolver>(x => x.GetPurchases(default, default));
            descriptor.Field<mainProductResolver>(x => x.GetShowcases(default, default));
            descriptor.Field<mainProductResolver>(x => x.GetOrders(default, default));
        }
        public class mainProductResolver
        {
            private readonly IproductCategoryService _productCategoryService;
            private readonly IproductUnitService _productUnitService;
            private readonly ImainPurchaseService _purchaseService;
            private readonly ImainShowcaseService _showcaseService;
            private readonly ImainOrderService _orderService;

            public mainProductResolver(
                [Service] IproductCategoryService productCategoryService,
                [Service] IproductUnitService productUnitService,
                [Service] ImainPurchaseService purchaseService,
                [Service] ImainShowcaseService showcaseService,
                [Service] ImainOrderService orderService
                )
            {
                _productCategoryService=productCategoryService;
                _productUnitService = productUnitService;
                _purchaseService = purchaseService;
                _showcaseService = showcaseService;
                _orderService = orderService;
            }
            public IEnumerable<mainOrder> GetOrders(mainProduct product, IResolverContext ctx)
            {
                return _orderService.GetAll().Where(p => p.ProductId == product.Id);
            }

            public productCategory GetproductCategory(mainProduct product, IResolverContext ctx)
            {
                return _productCategoryService.GetById(product.CategoryId);
            }
            public productUnit GetproductUnit(mainProduct product, IResolverContext ctx)
            {
                return _productUnitService.GetById(product.UnitId);
            }


            public IEnumerable<mainPurchase> GetPurchases(mainProduct product, IResolverContext ctx)
            {
                return _purchaseService.GetAll().Where(p => p.ProductId == product.Id);
            }
            public IEnumerable<mainShowcase> GetShowcases(mainProduct product, IResolverContext ctx)
            {
                return _showcaseService.GetAll().Where(p => p.ProductId == product.Id);
            }
        }
    }
}

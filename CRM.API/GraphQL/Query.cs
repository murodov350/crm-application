﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRM.API.Service;
using CRM.Database.Models;
using HotChocolate.Types;
using CRM.API.GraphQL.Type;
using HotChocolate.AspNetCore.Authorization;

namespace CRM.API.GraphQL
{
    public class Query
    {
        private readonly ImainCounterpartyService _counterpartyService;
        private readonly ImainManagerCounterpartyService _managerCounterpartyService;
        private readonly ImainManagerService _managerService;
        private readonly ImainFactoryService _factoryService;
        private readonly ImainManagerRoleService _managerRoleService;
        private readonly ImainMoneyService _moneyService;
        private readonly ImainMoneyCommentService _moneyCommentService;
        private readonly ImainOrderService _orderService;
        private readonly ImainOrderCommentService _orderCommentService;
        private readonly ImainPermissionService _permissionService;
        private readonly ImainProductService _productService;
        private readonly ImainPurchaseService _purchaseService;
        private readonly ImainReturnPurchaseService _returnPurchaseService;
        private readonly ImainSaleService _saleService;
        private readonly ImainReturnSaleService _returnSaleService;
        private readonly ImainRolesPermissionsService _rolesPermissionsService;
        private readonly ImainRolsService _rolsService;
        private readonly ImainSectionService _sectionService;
        private readonly ImainShowcaseService _showcaseService;
        private readonly ImainUsersService _usersService;
        private readonly ImainWarehouseService _warehouseService;
        private readonly ImoneyPaymentTypeService _paymentTypeService;
        private readonly ImoneyPurposePymentService _moneyPurposePymentService;
        private readonly IproductCategoryService _productCategoryService;
        private readonly IproductUnitService _productUnitService;

        public Query(ImainCounterpartyService counterpartyService,
            ImainManagerService managerService,
            ImainManagerCounterpartyService managerCounterpartyService, 
            ImainFactoryService factoryService, 
            ImainManagerRoleService managerRoleService, 
            ImainMoneyService moneyService, 
            ImainMoneyCommentService moneyCommentService, 
            ImainOrderService orderService, 
            ImainOrderCommentService orderCommentService, 
            ImainPermissionService permissionService, 
            ImainProductService productService, 
            ImainPurchaseService purchaseService, 
            ImainReturnPurchaseService returnPurchaseService, 
            ImainReturnSaleService returnSaleService, 
            ImainRolesPermissionsService rolesPermissionsService, 
            ImainRolsService rolsService, 
            ImainSaleService saleService, 
            ImainSectionService sectionService, 
            ImainShowcaseService showcaseService, 
            ImainUsersService usersService, 
            ImainWarehouseService warehouseService, 
            ImoneyPaymentTypeService paymentTypeService, 
            ImoneyPurposePymentService moneyPurposePymentService, 
            IproductCategoryService productCategoryService, 
            IproductUnitService productUnitService)
        {
            _managerCounterpartyService = managerCounterpartyService;
            _managerService = managerService;
            _counterpartyService = counterpartyService;
            _factoryService = factoryService;
            _managerRoleService = managerRoleService;
            _moneyService = moneyService;
            _moneyCommentService = moneyCommentService;
            _orderService = orderService;
            _orderCommentService = orderCommentService;
            _permissionService = permissionService;
            _productService = productService;
            _purchaseService = purchaseService;
            _returnPurchaseService = returnPurchaseService;
            _returnSaleService = returnSaleService;
            _rolesPermissionsService = rolesPermissionsService;
            _rolsService = rolsService;
            _saleService = saleService;
            _sectionService = sectionService;
            _showcaseService = showcaseService;
            _usersService = usersService;
            _warehouseService = warehouseService;
            _paymentTypeService = paymentTypeService;
            _moneyPurposePymentService = moneyPurposePymentService;
            _productCategoryService = productCategoryService;
            _productUnitService = productUnitService;
        }
        ///
        ///
        //[Authorize(Roles = new[] { "super-admin" })]
        [UsePaging(SchemaType = typeof(mainCounterpartyType))]
        [UseFiltering]
        [Authorize(Policy = "counterparties")]
        public IQueryable<mainCounterparty> counterparties => _counterpartyService.GetAll();

        [UsePaging(SchemaType = typeof(mainCounterpartyType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeletedCounterparty")]
        public IQueryable<mainCounterparty> AllDeletedCounterparty => _counterpartyService.GetAllDeleted();
        ///
        ///

        [UsePaging(SchemaType = typeof(mainFactoryType))]
        [UseFiltering]
        [Authorize(Policy = "factories")]
        public IQueryable<mainFactory> factories => _factoryService.GetAll();

        [UsePaging(SchemaType = typeof(mainFactoryType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeletedFactory")]
        public IQueryable<mainFactory> AllDeletedFactory => _factoryService.GetAllDeleted();
        ///
        ///
        [UsePaging(SchemaType = typeof(mainManagerCounterpartyType))]
        [UseFiltering]
        [Authorize(Policy = "managerCounterparties")]
        public IQueryable<mainManagerCounterparty> managerCounterparties => _managerCounterpartyService.GetAll();

        [UsePaging(SchemaType = typeof(mainManagerCounterpartyType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeleteManagerCounterparties")]
        public IQueryable<mainManagerCounterparty> AllDeleteManagerCounterparties => _managerCounterpartyService.GetAll();
        ///
        ///
        [UsePaging(SchemaType = typeof(mainManagerRoleType))]
        [UseFiltering]
        [Authorize(Policy = "managerRoles")]
        public IQueryable<mainManagerRole> managerRoles => _managerRoleService.GetAll();
        [UsePaging(SchemaType = typeof(mainManagerRoleType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeleteManagerRoles")]
        public IQueryable<mainManagerRole> AllDeleteManagerRoles => _managerRoleService.GetAllDeleted();
        ///
        ///
        [UsePaging(SchemaType = typeof(mainManagerType))]
        [UseFiltering]
        [Authorize(Policy = "managers")]
        public IQueryable<mainManager> managers => _managerService.GetAll();
        [UsePaging(SchemaType = typeof(mainManagerType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeleteManagers")]
        public IQueryable<mainManager> AllDeleteManagers => _managerService.GetAllDeleted();
        ///
        ///
        [UsePaging(SchemaType = typeof(mainMoneyCommentType))]
        [UseFiltering]
        [Authorize(Policy = "moneyComments")]
        public IQueryable<mainMoneyComment> moneyComments => _moneyCommentService.GetAll();

        [UsePaging(SchemaType = typeof(mainMoneyCommentType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeleteMoneyComments")]
        public IQueryable<mainMoneyComment> AllDeleteMoneyComments => _moneyCommentService.GetAllDeleted();
        ///
        ///
        [UsePaging(SchemaType = typeof(mainMoneyType))]
        [UseFiltering]
        [Authorize(Policy = "moneys")]
        public IQueryable<mainMoney> moneys => _moneyService.GetAll();
        [UsePaging(SchemaType = typeof(mainMoneyType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeleteMoneys")]
        public IQueryable<mainMoney> AllDeleteMoneys => _moneyService.GetAllDeleted();
        ///
        ///
        [UsePaging(SchemaType = typeof(mainOrderCommentType))]
        [UseFiltering]
        [Authorize(Policy = "mainOrderComments")]
        public IQueryable<mainOrderComment> mainOrderComments => _orderCommentService.GetAll();
        [UsePaging(SchemaType = typeof(mainOrderCommentType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeleteOrderComments")]
        public IQueryable<mainOrderComment> AllDeleteOrderComments => _orderCommentService.GetAllDeleted();
        ///
        ///
        [UsePaging(SchemaType = typeof(mainOrderType))]
        [UseFiltering]
        [Authorize(Policy = "mainOrders")]
        public IQueryable<mainOrder> mainOrders => _orderService.GetAll();

        [UsePaging(SchemaType = typeof(mainOrderType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeleteComments")]
        public IQueryable<mainOrder> AllDeleteComments => _orderService.GetAllDeleted();
        ///
        ///
        [UsePaging(SchemaType = typeof(mainPermissionType))]
        [UseFiltering]
        [Authorize(Policy = "permissions")]
        public IQueryable<mainPermission> permissions => _permissionService.GetAll();

        [UsePaging(SchemaType = typeof(mainPermissionType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeletePermissions")]
        public IQueryable<mainPermission> AllDeletePermissions => _permissionService.GetAllDeleted();
        ///
        ///
        [UsePaging(SchemaType = typeof(mainProductType))]
        [UseFiltering]
        [Authorize(Policy = "products")]
        public IQueryable<mainProduct> products => _productService.GetAll();
        [UsePaging(SchemaType = typeof(mainProductType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeleteProducts")]
        public IQueryable<mainProduct> AllDeleteProducts => _productService.GetAllDeleted();
        ///
        ///
        [UsePaging(SchemaType = typeof(mainPurchaseType))]
        [UseFiltering]
        [Authorize(Policy = "purchases")]
        public IQueryable<mainPurchase> purchases => _purchaseService.GetAll();
        [UsePaging(SchemaType = typeof(mainPurchaseType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeletePurchases")]
        public IQueryable<mainPurchase> AllDeletePurchases => _purchaseService.GetAllDeleted();
        ///
        ///
        [UsePaging(SchemaType = typeof(mainReturnPurchaseType))]
        [UseFiltering]
        [Authorize(Policy = "returnPurchases")]
        public IQueryable<mainReturnPurchase> returnPurchases => _returnPurchaseService.GetAll();
        [UsePaging(SchemaType = typeof(mainReturnPurchaseType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeleteReturPurchases")]
        public IQueryable<mainReturnPurchase> AllDeleteReturPurchases => _returnPurchaseService.GetAllDeleted();
        ///
        ///
        [UsePaging(SchemaType = typeof(mainReturnSaleType))]
        [UseFiltering]
        [Authorize(Policy = "returnSales")]
        public IQueryable<mainReturnSale> returnSales => _returnSaleService.GetAll();
        [UsePaging(SchemaType = typeof(mainReturnSaleType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeleteReturSales")]
        public IQueryable<mainReturnSale> AllDeleteReturSales => _returnSaleService.GetAllDeleted();
        ///
        ///
        [UsePaging(SchemaType = typeof(mainRolesPermissionsType))]
        [UseFiltering]
        [Authorize(Policy = "rolesPermissions")]
        public IQueryable<mainRolesPermissions> rolesPermissions => _rolesPermissionsService.GetAll();
        [UsePaging(SchemaType = typeof(mainRolesPermissionsType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeleteRolesPermissions")]
        public IQueryable<mainRolesPermissions> AllDeleteRolesPermissions => _rolesPermissionsService.GetAllDeleted();
        ///
        ///
        [UsePaging(SchemaType = typeof(mainRolsType))]
        [UseFiltering]
        [Authorize(Policy = "rols")]
        public IQueryable<mainRols> rols => _rolsService.GetAll();
        [UsePaging(SchemaType = typeof(mainRolsType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeleteRols")]
        public IQueryable<mainRols> AllDeleteRols => _rolsService.GetAllDeleted();
        ///
        ///
        [UsePaging(SchemaType = typeof(mainSaleType))]
        [UseFiltering]
        [Authorize(Policy = "sales")]
        public IQueryable<mainSale> sales => _saleService.GetAll();
        [UsePaging(SchemaType = typeof(mainSaleType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeleteSales")]
        public IQueryable<mainSale> AllDeleteSales => _saleService.GetAllDeleted();
        ///
        ///
        [UsePaging(SchemaType = typeof(mainSectionType))]
        [UseFiltering]
        [Authorize(Policy = "sections")]
        public IQueryable<mainSection> sections  => _sectionService.GetAll();
        [UsePaging(SchemaType = typeof(mainSectionType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeleteSections")]
        public IQueryable<mainSection> AllDeleteSections => _sectionService.GetAllDeleted();
        ///
        ///
        [UsePaging(SchemaType = typeof(mainShowcaseType))]
        [UseFiltering]
        [Authorize(Policy = "showcases")]
        public IQueryable<mainShowcase> showcases  => _showcaseService.GetAll();
        [UsePaging(SchemaType = typeof(mainShowcaseType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeleteShowcases")]
        public IQueryable<mainShowcase> AllDeleteShowcases => _showcaseService.GetAllDeleted();
        ///
        ///
        [UsePaging(SchemaType = typeof(mainUsersType))]
        [UseFiltering]
        [Authorize(Policy = "users")]
        public IQueryable<mainUsers> users  => _usersService.GetAll();
        [UsePaging(SchemaType = typeof(mainUsersType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeleteUsers")]
        public IQueryable<mainUsers> AllDeleteUsers => _usersService.GetAllDeleted();
        ///
        ///
        [UsePaging(SchemaType = typeof(mainWarehouseType))]
        [UseFiltering]
        [Authorize(Policy = "warehouses")]
        public IQueryable<mainWarehouse> warehouses  => _warehouseService.GetAll();
        [UsePaging(SchemaType = typeof(mainWarehouseType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeleteWarehouses")]
        public IQueryable<mainWarehouse> AllDeleteWarehouses => _warehouseService.GetAllDeleted();
        ///
        ///
        [UsePaging(SchemaType = typeof(moneyPaymentTypesType))]
        [UseFiltering]
        [Authorize(Policy = "paymentTypes")]
        public IQueryable<moneyPaymentType> paymentTypes  => _paymentTypeService.GetAll();
        [UsePaging(SchemaType = typeof(moneyPaymentTypesType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeletePaymentType")]
        public IQueryable<moneyPaymentType> AllDeletePaymentType => _paymentTypeService.GetAllDeleted();
        ///
        ///
        [UsePaging(SchemaType = typeof(moneyPurposePymentType))]
        [UseFiltering]
        [Authorize(Policy = "purposePyments")]
        public IQueryable<moneyPurposePyment> purposePyments  => _moneyPurposePymentService.GetAll();
        [UsePaging(SchemaType = typeof(moneyPurposePymentType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeletePurposePyments")]
        public IQueryable<moneyPurposePyment> AllDeletePurposePyments => _moneyPurposePymentService.GetAllDeleted();
        ///
        ///
        [UsePaging(SchemaType = typeof(productCategoryType))]
        [UseFiltering]
        [Authorize(Policy = "productCategories")]
        public IQueryable<productCategory> productCategories  => _productCategoryService.GetAll();
        [UsePaging(SchemaType = typeof(productCategoryType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeleteProductCategories")]
        public IQueryable<productCategory> AllDeleteProductCategories => _productCategoryService.GetAllDeleted();
        ///
        ///
        [UsePaging(SchemaType = typeof(productUnitType))]
        [UseFiltering]
        [Authorize(Policy = "productUnits")]
        public IQueryable<productUnit> productUnits  => _productUnitService.GetAll();
        [UsePaging(SchemaType = typeof(productUnitType))]
        [UseFiltering]
        [Authorize(Policy = "AllDeleteProductUnits")]
        public IQueryable<productUnit> AllDeleteProductUnits => _productUnitService.GetAllDeleted();
        ///
    }
}

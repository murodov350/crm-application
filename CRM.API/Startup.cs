using System;
using System.Text;
using CRM.API.GraphQL;
using CRM.API.GraphQL.Type;
using CRM.API.Logics;
using CRM.API.Service;
using CRM.API.Shared;
using CRM.Database;
using CRM.Database.Models;
using HotChocolate;
using HotChocolate.AspNetCore;
using HotChocolate.AspNetCore.Playground;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;

namespace CRM.API
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        [Obsolete]
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<TokenSettings>(Configuration.GetSection("TokenSettings"));

            services.AddTransient<mainFactory>();
            services.AddScoped<mainFactory>();

            services.AddTransient<ImainCounterpartyService, mainCounterpartyService>();
            services.AddTransient<ImainManagerService, mainManagerService>();
            services.AddTransient<ImainManagerCounterpartyService, mainManagerCounterpartyService>();
            services.AddTransient<ImainFactoryService, mainFactoryService>();
            services.AddTransient<ImainManagerRoleService, mainManagerRoleService>();
            services.AddTransient<ImainMoneyService, mainMoneyService>();
            services.AddTransient<ImainMoneyCommentService, mainMoneyCommentService>();
            services.AddTransient<ImainOrderService, mainOrderService>();
            services.AddTransient<ImainOrderCommentService, mainOrderCommentService>();
            services.AddTransient<ImainPermissionService, mainPermissionService>();
            services.AddTransient<ImainProductService, mainProductService>();
            services.AddTransient<ImainPurchaseService, mainPurchaseService>();
            services.AddTransient<ImainReturnPurchaseService, mainReturnPurchaseService>();
            services.AddTransient<ImainReturnSaleService, mainReturnSaleService>();
            services.AddTransient<ImainRolesPermissionsService, mainRolesPermissionsService>();
            services.AddTransient<ImainRolsService, mainRolsService>();
            services.AddTransient<ImainSaleService, mainSaleService>();
            services.AddTransient<ImainSectionService, mainSectionService>();
            services.AddTransient<ImainShowcaseService, mainShowcaseService>();
            services.AddTransient<ImainUsersService, mainUsersService>();
            services.AddTransient<ImainWarehouseService, mainWarehouseService>();
            services.AddTransient<ImoneyPaymentTypeService, moneyPaymentTypeService>();
            services.AddTransient<ImoneyPurposePymentService, moneyPurposePymentService>();
            services.AddTransient<IproductCategoryService, productCategoryService>();
            services.AddTransient<IproductUnitService, productUnitService>();

           

            services.AddScoped<Query>();
            services.AddScoped<Mutation>();
            services.AddScoped<IAuthLogic, AuthLogic>();
            services.AddDbContext<CRMContext>(options => options.UseSqlServer(Configuration["ConnectionStrings:dbCRM"]));

            services.AddGraphQLServer()
                .AddType<mainCounterpartyType>()
                .AddType<mainManagerType>()
                .AddType<mainManagerCounterpartyType>()
                .AddType<mainFactoryType>()
                .AddType<mainManagerRoleType>()
                .AddType<mainMoneyCommentType>()
                .AddType<mainMoneyType>()
                .AddType<mainOrderCommentType>()
                .AddType<mainOrderType>()
                .AddType<mainPermissionType>()
                .AddType<mainProductType>()
                .AddType<mainPurchaseType>()
                .AddType<mainReturnPurchaseType>()
                .AddType<mainReturnSaleType>()
                .AddType<mainRolesPermissionsType>()
                .AddType<mainRolsType>()
                .AddType<mainSaleType>()
                .AddType<mainSectionType>()
                .AddType<mainShowcaseType>()
                .AddType<mainUsersType>()
                .AddType<mainWarehouseType>()
                .AddType<moneyPaymentTypesType>()
                .AddType<moneyPurposePymentType>()
                .AddType<productCategoryType>()
                .AddType<productUnitType>()
                .AddQueryType<Query>()
                .AddMutationType<Mutation>()
                .AddAuthorization();

            #region secound type
            //services.AddGraphQL(x => SchemaBuilder.New()
            //    .AddServices(x)
            //    .AddType<mainCounterpartyType>()
            //    .AddType<mainManagerType>()
            //    .AddType<mainManagerCounterpartyType>()
            //    .AddType<mainFactoryType>()
            //    .AddType<mainManagerRoleType>()
            //    .AddType<mainMoneyCommentType>()
            //    .AddType<mainMoneyType>()
            //    .AddType<mainOrderCommentType>()
            //    .AddType<mainOrderType>()
            //    .AddType<mainPermissionType>()
            //    .AddType<mainProductType>()
            //    .AddType<mainPurchaseType>()
            //    .AddType<mainReturnPurchaseType>()
            //    .AddType<mainReturnSaleType>()
            //    .AddType<mainRolesPermissionsType>()
            //    .AddType<mainRolsType>()
            //    .AddType<mainSaleType>()
            //    .AddType<mainSectionType>()
            //    .AddType<mainShowcaseType>()
            //    .AddType<mainUsersType>()
            //    .AddType<mainWarehouseType>()
            //    .AddType<moneyPaymentTypesType>()
            //    .AddType<moneyPurposePymentType>()
            //    .AddType<productCategoryType>()
            //    .AddType<productUnitType>()
            //    .AddQueryType<Query>()
            //    .AddMutationType<Mutation>()
            //    .Create()
            //    );
            #endregion
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    var tokenSettings = Configuration
                    .GetSection("TokenSettings").Get<TokenSettings>();
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = tokenSettings.Issuer,
                        ValidateIssuer = true,
                        ValidAudience = tokenSettings.Audience,
                        ValidateAudience = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenSettings.Key)),
                        ValidateIssuerSigningKey = true
                    };
                });

            services.AddAuthorization(options => {

                #region Query Authorization
                options.AddPolicy("counterparties", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeletedCounterparty", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("factories", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeletedFactory", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("managerCounterparties", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeleteManagerCounterparties", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("managerRoles", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeleteManagerRoles", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("managers", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeleteManagers", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("moneyComments", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeleteMoneyComments", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("moneys", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeleteMoneys", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("mainOrderComments", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeleteOrderComments", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("mainOrders", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeleteComments", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("permissions", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeletePermissions", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("products", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeleteProducts", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("purchases", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeletePurchases", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("returnPurchases", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeleteReturPurchases", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("returnSales", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeleteReturSales", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("rolesPermissions", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeleteRolesPermissions", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("rols", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeleteRols", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("sales", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeleteSales", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("sections", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeleteSections", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("showcases", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeleteShowcases", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("users", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeleteUsers", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("warehouses", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeleteWarehouses", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("paymentTypes", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeletePaymentType", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("purposePyments", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeletePurposePyments", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("productCategories", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeleteProductCategories", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("productUnits", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("AllDeleteProductUnits", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                #endregion
                #region Mutation Authorization
                options.AddPolicy("CreateCounterparty", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdateCounterparty", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeleteCounterparty", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreateFactory", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdateFactory", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeleteFactory", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreateManager", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdateManager", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeleteManager", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreateManagerRole", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdateManagerRole", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeleteManagerRole", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreateManagerCounterparty", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdateManagerCounterparty", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeleteManagerCounterparty", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreateMoney", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdateMoney", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeleteMoney", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreateOrder", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdateOrder", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeleteOrder", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreateOrderComment", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdateOrderComment", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeleteOrderComment", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreatePermission", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdatePermission", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeletePermission", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreateProduct", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdateProduct", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeleteProduct", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreatePurchase", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdatePurchase", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeletePurchase", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreateReturnPurchase", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdateReturnPurchase", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeleteReturnPurchase", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreateSale", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdateSale", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeleteSale", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreateReturnSale", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdateReturnSale", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeleteReturnSale", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreateRolesPermissions", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdateRolesPermissions", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeleteRolesPermissions", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreateRols", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdateRols", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeleteRols", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreateSection", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdateSection", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeleteSection", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreateShowcase", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdateShowcase", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeleteShowcase", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreateUsers", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdateUsers", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeleteUsers", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreateWarehouse", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdateWarehouse", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeleteWarehouse", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreateMoneyComment", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdateMoneyComment", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeleteMoneyComment", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreateMoneyPaymentType", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdateMoneyPaymentType", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeleteMoneyPaymentType", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreateMoneyPurposePyment", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdateMoneyPurposePyment", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeleteMoneyPurposePyment", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreateProductCategory", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdateProductCategory", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeleteProductCategory", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("CreateProductUnit", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("UpdateProductUnit", policy => { policy.RequireRole(new string[] { "super-admin" }); });
                options.AddPolicy("DeleteProductUnit", policy => { policy.RequireRole(new string[] { "super-admin" }); });

                #endregion
                //options.AddPolicy("claim-policy-1", policy => {
                //    policy.RequireClaim("LastName");
                //});
                //options.AddPolicy("claim-policy-2", policy => {
                //    policy.RequireClaim("LastName", new string[] { "Bommidi", "Test" });
                //});
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        [Obsolete]
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                ////////1 type
                //app.UsePlayground(new PlaygroundOptions
                //{
                //    QueryPath = "/api",
                //    Path = "/graphql"
                //});
            }
            ///// 1 type
            //app.UseGraphQL("/api");
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                ///// 2 type
                endpoints.MapGraphQL();

                //endpoints.MapGet("/", async context =>
                //{
                //    await context.Response.WriteAsync("Hello World!");
                //});
            });
        }
    }
}

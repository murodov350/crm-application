﻿using CRM.Database;
using CRM.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface ImainOrderCommentService
    {
        IQueryable<mainOrderComment> GetAll();
        IQueryable<mainOrderComment> GetAllDeleted();
        mainOrderComment Create(orderCommentInput model);
        mainOrderComment Update(orderCommentInput model);
        mainOrderComment Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        mainOrderComment GetById(int? id);
    }
    public class mainOrderCommentService : ImainOrderCommentService
    {
        private readonly CRMContext _db;

        public mainOrderCommentService(CRMContext db)
        {
            _db = db;
        }

        public mainOrderComment Create(orderCommentInput model)
        {
            var result = new mainOrderComment()
            {
                Inserted = DateTime.Now,
                Updated = DateTime.Now,
                Deleted = 0,
                Status = 1,
                ManagerId = model.ManagerId,
                OrderId = model.OrderId,
                CommentsText = model.CommentsText
            };

            _db.mainOrderComment.Add(result);
            _db.SaveChanges();
            return result;
        }

        public mainOrderComment Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.mainOrderComment.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            if (result == null) return null;
            result.Deleted = 1;
            result.Updated = DateTime.Now;
            db.mainOrderComment.Update(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {

                var db = cRM.CreateDbContext(default);
                var result = db.mainOrderComment.FirstOrDefault(p => p.Id == item && p.Deleted != 1);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                result.Deleted = 1;
                result.Updated = DateTime.Now;
                db.mainOrderComment.Update(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<mainOrderComment> GetAll()
        {
            return _db.mainOrderComment.Where(p => p.Deleted != 1).AsQueryable();
        }

        public IQueryable<mainOrderComment> GetAllDeleted()
        {
            return _db.mainOrderComment.Where(p => p.Deleted == 1).AsQueryable();
        }

        public mainOrderComment GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.mainOrderComment.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            return result;
        }

        public mainOrderComment Update(orderCommentInput model)
        {
            throw new NotImplementedException();
        }
    }
}

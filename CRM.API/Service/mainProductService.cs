﻿using CRM.Database;
using CRM.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface ImainProductService
    {
        IQueryable<mainProduct> GetAll();
        IQueryable<mainProduct> GetAllDeleted();
        mainProduct Create(productInput model);
        mainProduct Update(productInput model);
        mainProduct Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        mainProduct GetById(int? id);
    }
    public class mainProductService : ImainProductService
    {
        private readonly CRMContext _db;

        public mainProductService(CRMContext db)
        {
            _db = db;
        }


        public mainProduct Create(productInput model)
        {
            var result = new mainProduct()
            {
                Inserted = DateTime.Now,
                Updated = DateTime.Now,
                Deleted = 0,
                Status = 1,
                NameModel = model.NameModel,
                NameParty = model.NameParty,
                Article = model.Article,
                CategoryId = model.CategoryId,
                UnitId = model.UnitId,
                PurchaseUSD = model.PurchaseUSD,
                CostPriceUSD = model.CostPriceUSD,
                SalePriceUSD = model.SalePriceUSD,
                Discount = model.Discount,
                Photo = model.Photo

            };

            _db.mainProduct.Add(result);
            _db.SaveChanges();
            return result;
        }

        public mainProduct Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.mainProduct.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            if (result == null) return null;
            result.Deleted = 1;
            result.Updated = DateTime.Now;
            db.mainProduct.Update(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {

                var db = cRM.CreateDbContext(default);
                var result = db.mainProduct.FirstOrDefault(p => p.Id == item && p.Deleted != 1);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                result.Deleted = 1;
                result.Updated = DateTime.Now;
                db.mainProduct.Update(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<mainProduct> GetAll()
        {
            return _db.mainProduct.Where(p => p.Deleted != 1).AsQueryable();
        }

        public IQueryable<mainProduct> GetAllDeleted()
        {
            return _db.mainProduct.Where(p => p.Deleted == 1).AsQueryable();
        }

        public mainProduct GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.mainProduct.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            return result;
        }

        public mainProduct Update(productInput model)
        {
            throw new NotImplementedException();
        }

    }
}

﻿using CRM.Database;
using CRM.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface ImainPermissionService
    {
        IQueryable<mainPermission> GetAll();
        IQueryable<mainPermission> GetAllDeleted();
        mainPermission Create(permissionInput model);
        mainPermission Update(permissionInput model);
        mainPermission Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        mainPermission GetById(int? id);
    }
    public class mainPermissionService : ImainPermissionService
    {
        private readonly CRMContext _db;

        public mainPermissionService(CRMContext db)
        {
            _db = db;
        }

        public mainPermission Create(permissionInput model)
        {
            var result = new mainPermission()
            {
                ResourceName = model.ResourceName,
                Comment = model.Comment,
                Name = model.Name
            };

            _db.mainPermission.Add(result);
            _db.SaveChanges();
            return result;
        }

        public mainPermission Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.mainPermission.FirstOrDefault(p => p.Id == id);
            if (result == null) return null;
            db.mainPermission.Remove(result);
            db.SaveChanges();
            return result;
        }
        
        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {

                var db = cRM.CreateDbContext(default);
                var result = db.mainPermission.FirstOrDefault(p => p.Id == item);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                db.mainPermission.Remove(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<mainPermission> GetAll()
        {
            return _db.mainPermission.AsQueryable();
        }

        public IQueryable<mainPermission> GetAllDeleted()
        {
            
            return null;
        }

        public mainPermission GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.mainPermission.FirstOrDefault(p => p.Id == id);
            return result;
        }

        public mainPermission Update(permissionInput model)
        {
            throw new NotImplementedException();
        }
    }
}

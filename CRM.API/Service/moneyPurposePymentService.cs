﻿using CRM.Database;
using CRM.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface ImoneyPurposePymentService
    {
        IQueryable<moneyPurposePyment> GetAll();
        IQueryable<moneyPurposePyment> GetAllDeleted();
        moneyPurposePyment Create(moneyPurposePymentInputs model);
        moneyPurposePyment Update(moneyPurposePymentInputs model);
        moneyPurposePyment Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        moneyPurposePyment GetById(int? id);
    }
    public class moneyPurposePymentService : ImoneyPurposePymentService
    {
        private readonly CRMContext _db;

        public moneyPurposePymentService(CRMContext db)
        {
            _db = db;
        }

        public moneyPurposePyment Create(moneyPurposePymentInputs model)
        {
            var result = new moneyPurposePyment()
            {
                Inserted = DateTime.Now,
                Updated = DateTime.Now,
                Deleted = 0,
                Status = 1,
                Name = model.Name

            };

            _db.moneyPurposePyment.Add(result);
            _db.SaveChanges();
            return result;
        }

        public moneyPurposePyment Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.moneyPurposePyment.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            if (result == null) return null;
            result.Deleted = 1;
            result.Updated = DateTime.Now;
            db.moneyPurposePyment.Update(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {
                var db = cRM.CreateDbContext(default);
                var result = db.moneyPurposePyment.FirstOrDefault(p => p.Id == item && p.Deleted != 1);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                result.Deleted = 1;
                result.Updated = DateTime.Now;
                db.moneyPurposePyment.Update(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<moneyPurposePyment> GetAll()
        {
            return _db.moneyPurposePyment.Where(p => p.Deleted != 1).AsQueryable();
        }

        public IQueryable<moneyPurposePyment> GetAllDeleted()
        {
            return _db.moneyPurposePyment.Where(p => p.Deleted == 1).AsQueryable();
        }

        public moneyPurposePyment GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.moneyPurposePyment.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            return result;
        }

        public moneyPurposePyment Update(moneyPurposePymentInputs model)
        {
            throw new NotImplementedException();
        }
    }
}

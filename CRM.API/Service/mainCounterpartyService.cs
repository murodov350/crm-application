﻿using CRM.Database;
using CRM.Database.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface ImainCounterpartyService
    {
        IQueryable<mainCounterparty> GetAll();
        IQueryable<mainCounterparty> GetAllDeleted();
        mainCounterparty Create(counterpartyInput counterparty);
        mainCounterparty Update(counterpartyInput counterparty);
        mainCounterparty Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        mainCounterparty GetById(int? id);
        
    }
    public class mainCounterpartyService : ImainCounterpartyService
    {
        private readonly CRMContext _db;
        public mainCounterpartyService(CRMContext db)
        {
            _db = db;
        }

        public mainCounterparty Create(counterpartyInput input)
        {
            var counterparty = new mainCounterparty()
            {
                Inserted = DateTime.Now,
                Updated = DateTime.Now,
                Deleted = 0,
                Status = 1,
                Lastname=input.Lastname,
                Name=input.Name,
                Middlename=input.Middlename,
                Phone=input.Phone,
                Address=input.Address

            };
            
            _db.mainCounterparty.Add(counterparty);
            _db.SaveChanges();
            return counterparty;
        }

        public mainCounterparty Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.mainCounterparty.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            if (result == null) return null;
            result.Deleted = 1;
            result.Updated = DateTime.Now;
            db.mainCounterparty.Update(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<Boolean> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {

                var db = cRM.CreateDbContext(default);
                var counterparty = db.mainCounterparty.FirstOrDefault(p => p.Id == item && p.Deleted != 1);
                if (counterparty == null)
                {
                    deleted.Add(false);
                    continue;
                }
                counterparty.Deleted = 1;
                counterparty.Updated = DateTime.Now;
                db.mainCounterparty.Update(counterparty);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<mainCounterparty> GetAll()
        {
            return _db.mainCounterparty.Where(p => p.Deleted != 1).AsQueryable();
        }

        public IQueryable<mainCounterparty> GetAllDeleted()
        {
            return _db.mainCounterparty.Where(p => p.Deleted == 1).AsQueryable();
        }

        public mainCounterparty GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c= cRM.CreateDbContext(default);
            var result = c.mainCounterparty.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            return result;
        }

        public mainCounterparty Update(counterpartyInput input)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.mainCounterparty.FirstOrDefault(predicate => predicate.Id == input.Id && predicate.Deleted != 1);
            if (result == null) return result;
            result.Lastname = input.Lastname;
            result.Name = input.Name;
            result.Middlename = input.Middlename;
            result.Phone = input.Phone;
            result.Address = input.Address;
            result.Updated = DateTime.Now;
            // set Modified flag in your entry
            db.Entry(result).State = EntityState.Modified;
            // save 
            db.SaveChanges();
            return result;
        }
    }
}

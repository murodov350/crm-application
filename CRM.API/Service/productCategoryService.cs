﻿using CRM.Database;
using CRM.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface IproductCategoryService
    {
        IQueryable<productCategory> GetAll();
        IQueryable<productCategory> GetAllDeleted();
        productCategory Create(productCategoryInputs model);
        productCategory Update(productCategoryInputs model);
        productCategory Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        productCategory GetById(int? id);
    }
    public class productCategoryService : IproductCategoryService
    {
        private readonly CRMContext _db;

        public productCategoryService(CRMContext db)
        {
            _db = db;
        }

        public productCategory Create(productCategoryInputs model)
        {
            var result = new productCategory()
            {
                Inserted = DateTime.Now,
                Updated = DateTime.Now,
                Deleted = 0,
                Status = 1,
                Name = model.Name

            };

            _db.productCategory.Add(result);
            _db.SaveChanges();
            return result;
        }

        public productCategory Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.productCategory.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            if (result == null) return null;
            result.Deleted = 1;
            result.Updated = DateTime.Now;
            db.productCategory.Update(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {
                var db = cRM.CreateDbContext(default);
                var result = db.productCategory.FirstOrDefault(p => p.Id == item && p.Deleted != 1);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                result.Deleted = 1;
                result.Updated = DateTime.Now;
                db.productCategory.Update(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<productCategory> GetAll()
        {
            return _db.productCategory.Where(p => p.Deleted != 1).AsQueryable();
        }

        public IQueryable<productCategory> GetAllDeleted()
        {
            return _db.productCategory.Where(p => p.Deleted == 1).AsQueryable();
        }

        public productCategory GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.productCategory.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            return result;
        }

        public productCategory Update(productCategoryInputs model)
        {
            throw new NotImplementedException();
        }
    }
}

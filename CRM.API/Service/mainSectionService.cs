﻿using CRM.Database;
using CRM.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface ImainSectionService
    {
        IQueryable<mainSection> GetAll();
        IQueryable<mainSection> GetAllDeleted();
        mainSection Create(sectionInput model);
        mainSection Update(sectionInput model);
        mainSection Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        mainSection GetById(int? id);
    }
    public class mainSectionService : ImainSectionService
    {
        private readonly CRMContext _db;

        public mainSectionService(CRMContext db)
        {
            _db = db;
        }

        public mainSection Create(sectionInput model)
        {
            var result = new mainSection()
            {
                Inserted = DateTime.Now,
                Updated = DateTime.Now,
                Deleted = 0,
                Status = 1,
                Name = model.Name

            };

            _db.mainSection.Add(result);
            _db.SaveChanges();
            return result;
        }

        public mainSection Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.mainSection.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            if (result == null) return null;
            result.Deleted = 1;
            result.Updated = DateTime.Now;
            db.mainSection.Update(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {
                var db = cRM.CreateDbContext(default);
                var result = db.mainSection.FirstOrDefault(p => p.Id == item && p.Deleted != 1);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                result.Deleted = 1;
                result.Updated = DateTime.Now;
                db.mainSection.Update(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<mainSection> GetAll()
        {
            return _db.mainSection.Where(p => p.Deleted != 1).AsQueryable();
        }

        public IQueryable<mainSection> GetAllDeleted()
        {
            return _db.mainSection.Where(p => p.Deleted == 1).AsQueryable();
        }

        public mainSection GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.mainSection.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            return result;
        }

        public mainSection Update(sectionInput model)
        {
            throw new NotImplementedException();
        }
    }
}

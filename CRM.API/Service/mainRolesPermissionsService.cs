﻿using CRM.Database;
using CRM.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface ImainRolesPermissionsService
    {
        IQueryable<mainRolesPermissions> GetAll();
        IQueryable<mainRolesPermissions> GetAllDeleted();
        mainRolesPermissions Create(rolesPermissionsInput model);
        mainRolesPermissions Update(rolesPermissionsInput model);
        mainRolesPermissions Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        mainRolesPermissions GetById(int? id);
    }
    public class mainRolesPermissionsService : ImainRolesPermissionsService
    {
        private readonly CRMContext _db;

        public mainRolesPermissionsService(CRMContext db)
        {
            _db = db;
        }

        public mainRolesPermissions Create(rolesPermissionsInput model)
        {
            var result = new mainRolesPermissions()
            {
                RolsId = model.RolsId,
                PermissionId = model.PermissionId,
                View = model.View,
                Create = model.Create,
                Edit = model.Edit,
                Delete = model.Delete
            };

            _db.mainRolesPermissions.Add(result);
            _db.SaveChanges();
            return result;
        }

        public mainRolesPermissions Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.mainRolesPermissions.FirstOrDefault(p => p.Id == id);
            if (result == null) return null;
            db.mainRolesPermissions.Remove(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {
                var db = cRM.CreateDbContext(default);
                var result = db.mainRolesPermissions.FirstOrDefault(p => p.Id == item);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                db.mainRolesPermissions.Remove(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<mainRolesPermissions> GetAll()
        {
            return _db.mainRolesPermissions.AsQueryable();
        }

        public IQueryable<mainRolesPermissions> GetAllDeleted()
        {
            return _db.mainRolesPermissions.AsQueryable();
        }

        public mainRolesPermissions GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.mainRolesPermissions.FirstOrDefault(p => p.Id.Equals(id));
            return result;
        }

        public mainRolesPermissions Update(rolesPermissionsInput model)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using CRM.Database;
using CRM.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface ImainRolsService
    {
        IQueryable<mainRols> GetAll();
        IQueryable<mainRols> GetAllDeleted();
        mainRols Create(rolsInput model);
        mainRols Update(rolsInput model);
        mainRols Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        mainRols GetById(int? id);
    }
    public class mainRolsService : ImainRolsService
    {
        private readonly CRMContext _db;

        public mainRolsService(CRMContext db)
        {
            _db = db;
        }

        public mainRols Create(rolsInput model)
        {
            var result = new mainRols()
            {
                ParentId = model.ParentId,
                ResourceName = model.ResourceName,
                Status = true

            };

            _db.mainRols.Add(result);
            _db.SaveChanges();
            return result;
        }

        public mainRols Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.mainRols.FirstOrDefault(p => p.Id == id);
            if (result == null) return null;
            db.mainRols.Remove(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {
                var db = cRM.CreateDbContext(default);
                var result = db.mainRols.FirstOrDefault(p => p.Id == item);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                db.mainRols.Remove(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<mainRols> GetAll()
        {
            return _db.mainRols.AsQueryable();
        }

        public IQueryable<mainRols> GetAllDeleted()
        {
            return null;
        }

        public mainRols GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.mainRols.FirstOrDefault(p => p.Id == id);
            return result;
        }

        public mainRols Update(rolsInput model)
        {
            throw new NotImplementedException();
        }
    }
}

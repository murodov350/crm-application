﻿using CRM.Database;
using CRM.Database.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface ImainFactoryService
    {
        IQueryable<mainFactory> GetAll();
        IQueryable<mainFactory> GetAllDeleted();
        mainFactory Create(mainFactory model);
        mainFactory Update(factoryInput model);
        mainFactory Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        mainFactory GetById(int? id);
    }
    public class mainFactoryService : ImainFactoryService
    {
        private readonly CRMContext _db;

        public mainFactoryService(CRMContext db)
        {
            _db = db;
        }

        public mainFactory Create(mainFactory input)
        {
            var result = new mainFactory()
            {
                Inserted = DateTime.Now,
                Updated = DateTime.Now,
                Deleted = 0,
                Status = 1,
                Name = input.Name,
                Phone = input.Phone,
                Email = input.Email,
                Address = input.Address

            };

            _db.mainFactory.Add(result);
            _db.SaveChanges();
            return result;
        }

        public mainFactory Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.mainFactory.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            if (result == null) return null;
            result.Deleted = 1;
            result.Updated = DateTime.Now;
            db.mainFactory.Update(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {

                var db = cRM.CreateDbContext(default);
                var result = db.mainFactory.FirstOrDefault(p => p.Id == item && p.Deleted != 1);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                result.Deleted = 1;
                result.Updated = DateTime.Now;
                db.mainFactory.Update(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<mainFactory> GetAll()
        {
            return _db.mainFactory.Where(p => p.Deleted != 1).AsQueryable();
        }

        public IQueryable<mainFactory> GetAllDeleted()
        {
            return _db.mainFactory.Where(p => p.Deleted == 1).AsQueryable();
        }

        public mainFactory GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.mainFactory.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            return result;
        }

        public mainFactory Update(factoryInput input)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.mainFactory.FirstOrDefault(predicate => predicate.Id == input.Id && predicate.Deleted != 1);
            if (result == null) return result;
            result.Name = input.Name;
            result.Phone = input.Phone;
            result.Email = input.Email;
            result.Address = input.Address;
            // set Modified flag in your entry
            db.Entry(result).State = EntityState.Modified;
            // save 
            db.SaveChanges();
            return result;
        }
    }
}

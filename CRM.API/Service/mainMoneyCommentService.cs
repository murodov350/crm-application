﻿using CRM.Database;
using CRM.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface ImainMoneyCommentService
    {
        IQueryable<mainMoneyComment> GetAll();
        IQueryable<mainMoneyComment> GetAllDeleted();
        mainMoneyComment Create(moneyCommentInput model);
        mainMoneyComment Update(moneyCommentInput model);
        mainMoneyComment Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        mainMoneyComment GetById(int? id);
    }
    public class mainMoneyCommentService : ImainMoneyCommentService
    {
        private readonly CRMContext _db;

        public mainMoneyCommentService(CRMContext db)
        {
            _db = db;
        }

        public mainMoneyComment Create(moneyCommentInput model)
        {
            var result = new mainMoneyComment()
            {
                Inserted = DateTime.Now,
                Updated = DateTime.Now,
                Deleted = 0,
                Status = 1,
                CounterpartyId = model.CounterpartyId,
                MoneyId = model.MoneyId,
                CommentsText= model.CommentsText

            };

            _db.mainMoneyComment.Add(result);
            _db.SaveChanges();
            return result;
        }

        public mainMoneyComment Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.mainMoneyComment.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            if (result == null) return null;
            result.Deleted = 1;
            result.Updated = DateTime.Now;
            db.mainMoneyComment.Update(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {

                var db = cRM.CreateDbContext(default);
                var result = db.mainMoneyComment.FirstOrDefault(p => p.Id == item && p.Deleted != 1);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                result.Deleted = 1;
                result.Updated = DateTime.Now;
                db.mainMoneyComment.Update(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<mainMoneyComment> GetAll()
        {
            return _db.mainMoneyComment.Where(p => p.Deleted != 1).AsQueryable();
        }

        public IQueryable<mainMoneyComment> GetAllDeleted()
        {
            return _db.mainMoneyComment.Where(p => p.Deleted == 1).AsQueryable();
        }

        public mainMoneyComment GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.mainMoneyComment.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            return result;
        }

        public mainMoneyComment Update(moneyCommentInput model)
        {
            throw new NotImplementedException();
        }
    }
}

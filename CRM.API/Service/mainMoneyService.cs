﻿using CRM.Database;
using CRM.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface ImainMoneyService
    {
        IQueryable<mainMoney> GetAll();
        IQueryable<mainMoney> GetAllDeleted();
        mainMoney Create(moneyInput model);
        mainMoney Update(moneyInput model);
        mainMoney Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        mainMoney GetById(int? id);
    }
    public class mainMoneyService : ImainMoneyService
    {
        private readonly CRMContext _db;

        public mainMoneyService(CRMContext db)
        {
            _db = db;
        }

        public mainMoney Create(moneyInput model)
        {
            var result = new mainMoney()
            {
                Inserted = DateTime.Now,
                Updated = DateTime.Now,
                Deleted = 0,
                Status = 1,
                Type = model.Type,
                DateMoney = model.DateMoney,
                WarehouseId = model.WarehouseId,
                CounterpartyId = model.CounterpartyId,
                PaymentTypeId = model.PaymentTypeId,
                ExchangeRate = model.ExchangeRate,
                AmmountUSD = model.AmmountUSD,
                PurposePymentId = model.PurposePymentId,
                UserId = model.UserId
            };

            _db.mainMoney.Add(result);
            _db.SaveChanges();
            return result;
        }

        public mainMoney Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.mainMoney.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            if (result == null) return null;
            result.Deleted = 1;
            result.Updated = DateTime.Now;
            db.mainMoney.Update(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {

                var db = cRM.CreateDbContext(default);
                var result = db.mainMoney.FirstOrDefault(p => p.Id == item && p.Deleted != 1);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                result.Deleted = 1;
                result.Updated = DateTime.Now;
                db.mainMoney.Update(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<mainMoney> GetAll()
        {
            return _db.mainMoney.Where(p => p.Deleted != 1).AsQueryable();
        }

        public IQueryable<mainMoney> GetAllDeleted()
        {
            return _db.mainMoney.Where(p => p.Deleted == 1).AsQueryable();
        }

        public mainMoney GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.mainMoney.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            return result;
        }

        public mainMoney Update(moneyInput model)
        {
            throw new NotImplementedException();
        }
    }
}

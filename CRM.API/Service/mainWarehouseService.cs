﻿using CRM.Database;
using CRM.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface ImainWarehouseService
    {
        IQueryable<mainWarehouse> GetAll();
        IQueryable<mainWarehouse> GetAllDeleted();
        mainWarehouse Create(warehouseInput model);
        mainWarehouse Update(warehouseInput model);
        mainWarehouse Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        mainWarehouse GetById(int? id);
    }
    public class mainWarehouseService : ImainWarehouseService
    {
        private readonly CRMContext _db;

        public mainWarehouseService(CRMContext db)
        {
            _db = db;
        }

        public mainWarehouse Create(warehouseInput model)
        {
            var result = new mainWarehouse()
            {
                Inserted = DateTime.Now,
                Updated = DateTime.Now,
                Deleted = 0,
                Status = 1,
                Name = model.Name,
                Address = model.Address
            };

            _db.mainWarehouse.Add(result);
            _db.SaveChanges();
            return result;
        }

        public mainWarehouse Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.mainWarehouse.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            if (result == null) return null;
            result.Deleted = 1;
            result.Updated = DateTime.Now;
            db.mainWarehouse.Update(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {
                var db = cRM.CreateDbContext(default);
                var result = db.mainWarehouse.FirstOrDefault(p => p.Id == item && p.Deleted != 1);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                result.Deleted = 1;
                result.Updated = DateTime.Now;
                db.mainWarehouse.Update(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<mainWarehouse> GetAll()
        {
            return _db.mainWarehouse.Where(p => p.Deleted != 1).AsQueryable();
        }

        public IQueryable<mainWarehouse> GetAllDeleted()
        {
            return _db.mainWarehouse.Where(p => p.Deleted == 1).AsQueryable();
        }

        public mainWarehouse GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.mainWarehouse.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            return result;
        }

        public mainWarehouse Update(warehouseInput model)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using CRM.Database;
using CRM.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface IproductUnitService
    {
        IQueryable<productUnit> GetAll();
        IQueryable<productUnit> GetAllDeleted();
        productUnit Create(productUnitInputs model);
        productUnit Update(productUnitInputs model);
        productUnit Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        productUnit GetById(int? id);
    }
    public class productUnitService : IproductUnitService
    {
        private readonly CRMContext _db;

        public productUnitService(CRMContext db)
        {
            _db = db;
        }

        public productUnit Create(productUnitInputs model)
        {
            var result = new productUnit()
            {
                Inserted = DateTime.Now,
                Updated = DateTime.Now,
                Deleted = 0,
                Status = 1,
                Name = model.Name

            };

            _db.productUnit.Add(result);
            _db.SaveChanges();
            return result;
        }

        public productUnit Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.productUnit.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            if (result == null) return null;
            result.Deleted = 1;
            result.Updated = DateTime.Now;
            db.productUnit.Update(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {
                var db = cRM.CreateDbContext(default);
                var result = db.productUnit.FirstOrDefault(p => p.Id == item && p.Deleted != 1);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                result.Deleted = 1;
                result.Updated = DateTime.Now;
                db.productUnit.Update(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<productUnit> GetAll()
        {
            return _db.productUnit.Where(p => p.Deleted != 1).AsQueryable();
        }

        public IQueryable<productUnit> GetAllDeleted()
        {
            return _db.productUnit.Where(p => p.Deleted == 1).AsQueryable();
        }

        public productUnit GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.productUnit.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            return result;
        }

        public productUnit Update(productUnitInputs model)
        {
            throw new NotImplementedException();
        }
    }
}

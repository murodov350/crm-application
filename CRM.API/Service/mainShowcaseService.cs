﻿using CRM.Database;
using CRM.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface ImainShowcaseService
    {
        IQueryable<mainShowcase> GetAll();
        IQueryable<mainShowcase> GetAllDeleted();
        mainShowcase Create(showcaseInput model);
        mainShowcase Update(showcaseInput model);
        mainShowcase Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        mainShowcase GetById(int? id);
    }
    public class mainShowcaseService : ImainShowcaseService
    {
        private readonly CRMContext _db;

        public mainShowcaseService(CRMContext db)
        {
            _db = db;
        }

        public mainShowcase Create(showcaseInput model)
        {
            var result = new mainShowcase()
            {
                Inserted = DateTime.Now,
                Updated = DateTime.Now,
                Deleted = 0,
                Status = 1,
                ProductId = model.ProductId,
                Count = model.Count,
                Article = model.Article,
                CounterpartyId = model.CounterpartyId

            };

            _db.mainShowcase.Add(result);
            _db.SaveChanges();
            return result;
        }

        public mainShowcase Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.mainShowcase.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            if (result == null) return null;
            result.Deleted = 1;
            result.Updated = DateTime.Now;
            db.mainShowcase.Update(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {
                var db = cRM.CreateDbContext(default);
                var result = db.mainShowcase.FirstOrDefault(p => p.Id == item && p.Deleted != 1);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                result.Deleted = 1;
                result.Updated = DateTime.Now;
                db.mainShowcase.Update(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<mainShowcase> GetAll()
        {
            return _db.mainShowcase.Where(p => p.Deleted != 1).AsQueryable();
        }

        public IQueryable<mainShowcase> GetAllDeleted()
        {
            return _db.mainShowcase.Where(p => p.Deleted == 1).AsQueryable();
        }

        public mainShowcase GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.mainShowcase.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            return result;
        }

        public mainShowcase Update(showcaseInput model)
        {
            throw new NotImplementedException();
        }
    }
}

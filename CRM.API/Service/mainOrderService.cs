﻿using CRM.Database;
using CRM.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface ImainOrderService
    {
        IQueryable<mainOrder> GetAll();
        IQueryable<mainOrder> GetAllDeleted();
        mainOrder Create(orderInput model);
        mainOrder Update(orderInput model);
        mainOrder Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        mainOrder GetById(int? id);
    }
    public class mainOrderService : ImainOrderService
    {
        private readonly CRMContext _db;

        public mainOrderService(CRMContext db)
        {
            _db = db;
        }

        public mainOrder Create(orderInput model)
        {
            var result = new mainOrder()
            {
                Inserted = DateTime.Now,
                Updated = DateTime.Now,
                Deleted = 0,
                Status = 1,
            };

            _db.mainOrder.Add(result);
            _db.SaveChanges();
            return result;
        }

        public mainOrder Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.mainOrder.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            if (result == null) return null;
            result.Deleted = 1;
            result.Updated = DateTime.Now;
            db.mainOrder.Update(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {

                var db = cRM.CreateDbContext(default);
                var result = db.mainOrder.FirstOrDefault(p => p.Id == item && p.Deleted != 1);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                result.Deleted = 1;
                result.Updated = DateTime.Now;
                db.mainOrder.Update(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<mainOrder> GetAll()
        {
            return _db.mainOrder.Where(p => p.Deleted != 1).AsQueryable();
        }

        public IQueryable<mainOrder> GetAllDeleted()
        {
            return _db.mainOrder.Where(p => p.Deleted == 1).AsQueryable();
        }

        public mainOrder GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.mainOrder.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            return result;
        }

        public mainOrder Update(orderInput model)
        {
            throw new NotImplementedException();
        }
    }
}

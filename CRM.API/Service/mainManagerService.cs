﻿using CRM.Database;
using CRM.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface ImainManagerService
    {
        IQueryable<mainManager> GetAll();
        IQueryable<mainManager> GetAllDeleted();
        mainManager Create(managerInput manager);
        mainManager Update(managerInput manager);
        mainManager Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        mainManager GetById(int? id);
    }
    public class mainManagerService : ImainManagerService
    {
        private readonly CRMContext _db;
        public mainManagerService(CRMContext db)
        {
            _db = db;
        }
        public mainManager Create(managerInput input)
        {
            var result = new mainManager()
            {
                Inserted = DateTime.Now,
                Updated = DateTime.Now,
                Deleted = 0,
                Status = 1,
                Lastname = input.Lastname,
                Name = input.Name,
                Middlename = input.Middlename,
                Phone = input.Phone,
                Address = input.Address

            };

            _db.mainManager.Add(result);
            _db.SaveChanges();
            return result;
        }

        public mainManager Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.mainManager.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            if (result == null) return null;
            result.Deleted = 1;
            result.Updated = DateTime.Now;
            db.mainManager.Update(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {

                var db = cRM.CreateDbContext(default);
                var result = db.mainManager.FirstOrDefault(p => p.Id == item && p.Deleted != 1);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                result.Deleted = 1;
                result.Updated = DateTime.Now;
                db.mainManager.Update(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<mainManager> GetAll()
        {
            return _db.mainManager.Where(p => p.Deleted != 1).AsQueryable();
        }

        public IQueryable<mainManager> GetAllDeleted()
        {
            return _db.mainManager.Where(p=>p.Deleted==1).AsQueryable();
        }

        public mainManager GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.mainManager.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            return result;

        }

        public mainManager Update(managerInput input)
        {
            throw new NotImplementedException();
        }
    }
}

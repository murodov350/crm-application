﻿using CRM.Database;
using CRM.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface ImainManagerRoleService
    {
        IQueryable<mainManagerRole> GetAll();
        IQueryable<mainManagerRole> GetAllDeleted();
        mainManagerRole Create(managerRoleInput model);
        mainManagerRole Update(managerRoleInput model);
        mainManagerRole Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        mainManagerRole GetById(int? id);
    }
    public class mainManagerRoleService : ImainManagerRoleService
    {
        private readonly CRMContext _db;

        public mainManagerRoleService(CRMContext db)
        {
            _db = db;
        }

        public mainManagerRole Create(managerRoleInput model)
        {
            var result = new mainManagerRole()
            {
                Inserted = DateTime.Now,
                Updated = DateTime.Now,
                Deleted = 0,
                Status = 1,
                ManagerId = model.ManagerId,
                SectionId = model.SectionId

            };

            _db.mainManagerRole.Add(result);
            _db.SaveChanges();
            return result;
        }

        public mainManagerRole Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.mainManagerRole.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            if (result == null) return null;
            result.Deleted = 1;
            result.Updated = DateTime.Now;
            db.mainManagerRole.Update(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {

                var db = cRM.CreateDbContext(default);
                var result = db.mainManagerRole.FirstOrDefault(p => p.Id == item && p.Deleted != 1);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                result.Deleted = 1;
                result.Updated = DateTime.Now;
                db.mainManagerRole.Update(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<mainManagerRole> GetAll()
        {
            return _db.mainManagerRole.Where(p => p.Deleted != 1).AsQueryable();
        }

        public IQueryable<mainManagerRole> GetAllDeleted()
        {
            return _db.mainManagerRole.Where(p => p.Deleted != 1).AsQueryable();
        }

        public mainManagerRole GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.mainManagerRole.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            return result;
        }

        public mainManagerRole Update(managerRoleInput model)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using CRM.Database;
using CRM.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface ImainReturnPurchaseService
    {
        IQueryable<mainReturnPurchase> GetAll();
        IQueryable<mainReturnPurchase> GetAllDeleted();
        mainReturnPurchase Create(returnPurchaseInput model);
        mainReturnPurchase Update(returnPurchaseInput model);
        mainReturnPurchase Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        mainReturnPurchase GetById(int? id);
    }
    public class mainReturnPurchaseService : ImainReturnPurchaseService
    {
        private readonly CRMContext _db;

        public mainReturnPurchaseService(CRMContext db)
        {
            _db = db;
        }

        public mainReturnPurchase Create(returnPurchaseInput model)
        {
            var result = new mainReturnPurchase()
            {
                Inserted = DateTime.Now,
                Updated = DateTime.Now,
                Deleted = 0,
                Status = 1,
                PriceUSD = model.PriceUSD,
                PurchaseId = model.PurchaseId,
                Amount = model.Amount,
                Document = model.Document,
                WarehouseId = model.WarehouseId,
                DateReturnPurchase = model.DateReturnPurchase,
                FactoryId = model.FactoryId,
                UsersId = model.UsersId                
            };

            _db.mainReturnPurchase.Add(result);
            _db.SaveChanges();
            return result;
        }

        public mainReturnPurchase Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.mainReturnPurchase.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            if (result == null) return null;
            result.Deleted = 1;
            result.Updated = DateTime.Now;
            db.mainReturnPurchase.Update(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {
                var db = cRM.CreateDbContext(default);
                var result = db.mainReturnPurchase.FirstOrDefault(p => p.Id == item && p.Deleted != 1);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                result.Deleted = 1;
                result.Updated = DateTime.Now;
                db.mainReturnPurchase.Update(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<mainReturnPurchase> GetAll()
        {
            return _db.mainReturnPurchase.Where(p => p.Deleted != 1).AsQueryable();
        }

        public IQueryable<mainReturnPurchase> GetAllDeleted()
        {
            return _db.mainReturnPurchase.Where(p => p.Deleted == 1).AsQueryable();
        }

        public mainReturnPurchase GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.mainReturnPurchase.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            return result;
        }

        public mainReturnPurchase Update(returnPurchaseInput model)
        {
            throw new NotImplementedException();
        }
    }
}

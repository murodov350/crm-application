﻿using CRM.Database;
using CRM.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface ImainSaleService
    {
        IQueryable<mainSale> GetAll();
        IQueryable<mainSale> GetAllDeleted();
        mainSale Create(saleInput model);
        mainSale Update(saleInput model);
        mainSale Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        mainSale GetById(int? id);
    }
    public class mainSaleService : ImainSaleService
    {
        private readonly CRMContext _db;

        public mainSaleService(CRMContext db)
        {
            _db = db;
        }

        public mainSale Create(saleInput model)
        {
            var result = new mainSale()
            {
                Inserted = DateTime.Now,
                Updated = DateTime.Now,
                Deleted = 0,
                Status = 1,
                DateSale = model.DateSale,
                Discount = model.Discount,
                UsersId = model.UsersId,
                WarehouseId = model.WarehouseId,
                CounterpartyId = model.CounterpartyId,
                Amount = model.Amount,
                PriceUSD = model.PriceUSD,
                PriceUZS = model.PriceUZS,
                Document = model.Document,
                ShowcaseId = model.ShowcaseId
            };

            _db.mainSale.Add(result);
            _db.SaveChanges();
            return result;
        }

        public mainSale Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.mainSale.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            if (result == null) return null;
            result.Deleted = 1;
            result.Updated = DateTime.Now;
            db.mainSale.Update(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {
                var db = cRM.CreateDbContext(default);
                var result = db.mainSale.FirstOrDefault(p => p.Id == item && p.Deleted != 1);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                result.Deleted = 1;
                result.Updated = DateTime.Now;
                db.mainSale.Update(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<mainSale> GetAll()
        {
            return _db.mainSale.Where(p => p.Deleted != 1).AsQueryable();
        }

        public IQueryable<mainSale> GetAllDeleted()
        {
            return _db.mainSale.Where(p => p.Deleted == 1).AsQueryable();
        }

        public mainSale GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.mainSale.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            return result;
        }

        public mainSale Update(saleInput model)
        {
            throw new NotImplementedException();
        }
    }
}

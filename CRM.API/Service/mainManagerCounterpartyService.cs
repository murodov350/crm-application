﻿using CRM.Database;
using CRM.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface ImainManagerCounterpartyService
    {
        IQueryable<mainManagerCounterparty> GetAll();
        IQueryable<mainManagerCounterparty> GetAllDeleted();
        mainManagerCounterparty Create(managerCounterpartyInput model);
        mainManagerCounterparty Update(managerCounterpartyInput model);
        mainManagerCounterparty Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        mainManagerCounterparty GetById(int? id);
    }
    public class mainManagerCounterpartyService : ImainManagerCounterpartyService
    {
        private readonly CRMContext _db;
        public mainManagerCounterpartyService(CRMContext db)
        {
            _db = db;
        }

        public mainManagerCounterparty Create(managerCounterpartyInput input)
        {

            var result = new mainManagerCounterparty()
            {
                Inserted = DateTime.Now,
                Updated = DateTime.Now,
                Deleted = 0,
                Status = 1,
                ManagerId = input.ManagerId,
                CounterpartyId = input.CounterpartyId,

            };

            _db.mainManagerCounterparty.Add(result);
            _db.SaveChanges();
            return result;
        }

        public mainManagerCounterparty Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.mainManagerCounterparty.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            if (result == null) return null;
            result.Deleted = 1;
            result.Updated = DateTime.Now;
            db.mainManagerCounterparty.Update(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {

                var db = cRM.CreateDbContext(default);
                var result = db.mainManagerCounterparty.FirstOrDefault(p => p.Id == item && p.Deleted != 1);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                result.Deleted = 1;
                result.Updated = DateTime.Now;
                db.mainManagerCounterparty.Update(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<mainManagerCounterparty> GetAll()
        {
            return _db.mainManagerCounterparty.Where(p => p.Deleted != 1).AsQueryable();
        }

        public IQueryable<mainManagerCounterparty> GetAllDeleted()
        {
            return _db.mainManagerCounterparty.Where(p => p.Deleted == 1).AsQueryable();
        }

        public mainManagerCounterparty GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.mainManagerCounterparty.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            return result;
        }

        public mainManagerCounterparty Update(managerCounterpartyInput model)
        {
            throw new NotImplementedException();
        }
    }
}

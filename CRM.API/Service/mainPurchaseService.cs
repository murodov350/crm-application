﻿using CRM.Database;
using CRM.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface ImainPurchaseService
    {
        IQueryable<mainPurchase> GetAll();
        IQueryable<mainPurchase> GetAllDeleted();
        mainPurchase Create(purchaseInput model);
        mainPurchase Update(purchaseInput model);
        mainPurchase Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        mainPurchase GetById(int? id);
    }
    public class mainPurchaseService : ImainPurchaseService
    {
        private readonly CRMContext _db;

        public mainPurchaseService(CRMContext db)
        {
            _db = db;
        }

        public mainPurchase Create(purchaseInput model)
        {
            var result = new mainPurchase()
            {
                Inserted = DateTime.Now,
                Updated = DateTime.Now,
                Deleted = 0,
                Status = 1,
                ProductId = model.ProductId,
                DatePurchase = model.DatePurchase,
                Document = model.Document,
                UsersId = model.UsersId,
                CounterpartyId = model.CounterpartyId,
                FactoryId = model.FactoryId,
                Amount = model.Amount,
                PriceUSD = model.PriceUSD

            };

            _db.mainPurchase.Add(result);
            _db.SaveChanges();
            return result;
        }

        public mainPurchase Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.mainPurchase.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            if (result == null) return null;
            result.Deleted = 1;
            result.Updated = DateTime.Now;
            db.mainPurchase.Update(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {

                var db = cRM.CreateDbContext(default);
                var result = db.mainPurchase.FirstOrDefault(p => p.Id == item && p.Deleted != 1);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                result.Deleted = 1;
                result.Updated = DateTime.Now;
                db.mainPurchase.Update(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<mainPurchase> GetAll()
        {
            return _db.mainPurchase.Where(p => p.Deleted != 1).AsQueryable();
        }

        public IQueryable<mainPurchase> GetAllDeleted()
        {
            return _db.mainPurchase.Where(p => p.Deleted == 1).AsQueryable();
        }

        public mainPurchase GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.mainPurchase.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            return result;
        }

        public mainPurchase Update(purchaseInput model)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using CRM.Database;
using CRM.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface ImainUsersService
    {
        IQueryable<mainUsers> GetAll();
        IQueryable<mainUsers> GetAllDeleted();
        mainUsers Create(usersInput model);
        mainUsers Update(usersInput model);
        mainUsers Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        mainUsers GetById(int? id);
    }
    public class mainUsersService : ImainUsersService
    {
        private readonly CRMContext _db;

        public mainUsersService(CRMContext db)
        {
            _db = db;
        }

        public mainUsers Create(usersInput model)
        {
            var result = new mainUsers()
            {
                Inserted = DateTime.Now,
                Updated = DateTime.Now,
                Deleted = 0,
                Status = 1,
                ManagerId = model.ManagerId,
                CounterpartyId = model.CounterpartyId,
                Login = model.Login,
                Password = model.Password,
                Rol = model.Rol

            };

            _db.mainUsers.Add(result);
            _db.SaveChanges();
            return result;
        }

        public mainUsers Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.mainUsers.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            if (result == null) return null;
            result.Deleted = 1;
            result.Updated = DateTime.Now;
            db.mainUsers.Update(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {
                var db = cRM.CreateDbContext(default);
                var result = db.mainUsers.FirstOrDefault(p => p.Id == item && p.Deleted != 1);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                result.Deleted = 1;
                result.Updated = DateTime.Now;
                db.mainUsers.Update(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<mainUsers> GetAll()
        {
            return _db.mainUsers.Where(p => p.Deleted != 1).AsQueryable();
        }

        public IQueryable<mainUsers> GetAllDeleted()
        {
            return _db.mainUsers.Where(p => p.Deleted == 1).AsQueryable();
        }

        public mainUsers GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.mainUsers.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            return result;
        }

        public mainUsers Update(usersInput model)
        {
            throw new NotImplementedException();
        }
    }
}

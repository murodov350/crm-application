﻿using CRM.API.GraphQL.Type;
using CRM.Database;
using CRM.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface ImoneyPaymentTypeService
    {
        IQueryable<moneyPaymentType> GetAll();
        IQueryable<moneyPaymentType> GetAllDeleted();
        moneyPaymentType Create(moneyPaymentTypeInputs model);
        moneyPaymentType Update(moneyPaymentTypeInputs model);
        moneyPaymentType Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        moneyPaymentType GetById(int? id);
    }
    public class moneyPaymentTypeService : ImoneyPaymentTypeService
    {
        private readonly CRMContext _db;

        public moneyPaymentTypeService(CRMContext db)
        {
            _db = db;
        }

        public moneyPaymentType Create(moneyPaymentTypeInputs model)
        {
            var result = new moneyPaymentType()
            {
                Inserted = DateTime.Now,
                Updated = DateTime.Now,
                Deleted = 0,
                Status = 1,
                Name = model.Name

            };

            _db.moneyPaymentType.Add(result);
            _db.SaveChanges();
            return result;
        }

        public moneyPaymentType Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.moneyPaymentType.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            if (result == null) return null;
            result.Deleted = 1;
            result.Updated = DateTime.Now;
            db.moneyPaymentType.Update(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {
                var db = cRM.CreateDbContext(default);
                var result = db.moneyPaymentType.FirstOrDefault(p => p.Id == item && p.Deleted != 1);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                result.Deleted = 1;
                result.Updated = DateTime.Now;
                db.moneyPaymentType.Update(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<moneyPaymentType> GetAll()
        {
            return _db.moneyPaymentType.Where(p => p.Deleted != 1).AsQueryable();
        }

        public IQueryable<moneyPaymentType> GetAllDeleted()
        {
            return _db.moneyPaymentType.Where(p => p.Deleted == 1).AsQueryable();
        }

        public moneyPaymentType GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.moneyPaymentType.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            return result;
        }

        public moneyPaymentType Update(moneyPaymentTypeInputs model)
        {
            throw new NotImplementedException();
        }
    }
}

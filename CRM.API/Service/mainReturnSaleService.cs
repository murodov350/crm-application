﻿using CRM.Database;
using CRM.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Service
{
    public interface ImainReturnSaleService
    {
        IQueryable<mainReturnSale> GetAll();
        IQueryable<mainReturnSale> GetAllDeleted();
        mainReturnSale Create(returnSaleInput model);
        mainReturnSale Update(returnSaleInput model);
        mainReturnSale Delete(int? id);
        IEnumerable<Boolean> DeleteSelection(List<int> ids);
        mainReturnSale GetById(int? id);
    }
    public class mainReturnSaleService : ImainReturnSaleService
    {
        private readonly CRMContext _db;

        public mainReturnSaleService(CRMContext db)
        {
            _db = db;
        }

        public mainReturnSale Create(returnSaleInput model)
        {
            var result = new mainReturnSale()
            {
                Inserted = DateTime.Now,
                Updated = DateTime.Now,
                Deleted = 0,
                Status = 1,
                DateReturnSale = model.DateReturnSale,
                Document = model.Document,
                UsersId = model.UsersId,
                FactoryId = model.FactoryId,
                WarehouseId = model.WarehouseId,
                Amount = model.Amount,
                PriceUSD = model.PriceUSD,
                PriceUZS = model.PriceUZS
            };

            _db.mainReturnSale.Add(result);
            _db.SaveChanges();
            return result;
        }

        public mainReturnSale Delete(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var db = cRM.CreateDbContext(default);
            var result = db.mainReturnSale.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            if (result == null) return null;
            result.Deleted = 1;
            result.Updated = DateTime.Now;
            db.mainReturnSale.Update(result);
            db.SaveChanges();
            return result;
        }

        public IEnumerable<bool> DeleteSelection(List<int> ids)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            List<Boolean> deleted = new List<bool>();
            foreach (var item in ids)
            {
                var db = cRM.CreateDbContext(default);
                var result = db.mainReturnSale.FirstOrDefault(p => p.Id == item && p.Deleted != 1);
                if (result == null)
                {
                    deleted.Add(false);
                    continue;
                }
                result.Deleted = 1;
                result.Updated = DateTime.Now;
                db.mainReturnSale.Update(result);
                db.SaveChanges();
                deleted.Add(true);
            }
            return deleted.AsEnumerable();
        }

        public IQueryable<mainReturnSale> GetAll()
        {
            return _db.mainReturnSale.Where(p => p.Deleted != 1).AsQueryable();
        }

        public IQueryable<mainReturnSale> GetAllDeleted()
        {
            return _db.mainReturnSale.Where(p => p.Deleted != 1).AsQueryable();
        }

        public mainReturnSale GetById(int? id)
        {
            CRMContextFactory cRM = new CRMContextFactory();
            var c = cRM.CreateDbContext(default);
            var result = c.mainReturnSale.FirstOrDefault(p => p.Id == id && p.Deleted != 1);
            return result;
        }

        public mainReturnSale Update(returnSaleInput model)
        {
            throw new NotImplementedException();
        }
    }
}

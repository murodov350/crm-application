﻿using CRM.API.InputTypes;
using CRM.API.Shared;
using CRM.Database;
using CRM.Database.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CRM.API.Logics
{
	public class AuthLogic : IAuthLogic
	{
		private readonly CRMContext _authContext;
		private readonly TokenSettings _tokenSettings;

		public AuthLogic(CRMContext authContext, IOptions<TokenSettings> tokenSettings)
		{
			_authContext = authContext;
			_tokenSettings = tokenSettings.Value;
		}
        private string ResigstrationValidations(RegisterInputType registerInput)
        {
            if (string.IsNullOrEmpty(registerInput.Email))
            {
                return "Eamil can't be empty";
            }
			if (string.IsNullOrEmpty(registerInput.Login))
            {
                return "Login can't be empty";
            }

            if (string.IsNullOrEmpty(registerInput.Password)
                || string.IsNullOrEmpty(registerInput.ConfirmPassword))
            {
                return "Password Or ConfirmPasswor Can't be empty";
            }

            if (registerInput.Password != registerInput.ConfirmPassword)
            {
                return "Invalid confirm password";
            }

            string loginRules = @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
            if (!Regex.IsMatch(registerInput.Login, loginRules))
            {
                return "Not a valid login";
            }
			var login = _authContext.mainUsers.Where(p => p.Login == registerInput.Login);
			if (login.Count() != 0) 
            {
				return "A user is registered with such a login!!";
			}
            // atleast one lower case letter
            // atleast one upper case letter
            // atleast one special character
            // atleast one number
            // atleast 8 character length
            string passwordRules = @"^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!*@#$%^&+=]).*$";
            if (!Regex.IsMatch(registerInput.Password, passwordRules))
            {
                return "Not a valid password";
            }

            return string.Empty;
        }

        public string Register(RegisterInputType registerInput)
        {
            string errorMessage = ResigstrationValidations(registerInput);
            if (!string.IsNullOrEmpty(errorMessage))
            {
                return errorMessage;
            }

            var newUser = new mainUsers
            { 
				Login=registerInput.Login,
                Password = PasswordHash(registerInput.ConfirmPassword),
				Deleted=0,
				Inserted=DateTime.Now,
				Updated=DateTime.Now,
				Status=1,
				Rol=2
            };
			var counterparty = new mainCounterparty()
			{
				Lastname = registerInput.Lastname,
				Name = registerInput.Name,
				Middlename = registerInput.Middlename,
				Address = registerInput.Address,
				Email = registerInput.Email,
				Phone = registerInput.Phone,
				Inserted = DateTime.Now,
				Updated = DateTime.Now,
				Deleted = 0,
				Status = 1
			};
			_authContext.mainCounterparty.Add(counterparty);
			_authContext.SaveChanges();
			var cId = _authContext.mainCounterparty.OrderByDescending(_ => _.Id).Take(1).Single().Id;
			newUser.CounterpartyId = cId;
            _authContext.mainUsers.Add(newUser);
            _authContext.SaveChanges();

            // default role on registration
            //var newUserRoles = new mRoles
            //{
            //    Name = "admin",
            //    UserId = newUser.UserId
            //};

            //_authContext.UserRoles.Add(newUserRoles);
            //_authContext.SaveChanges();

            return "Registration success";
        }

        private string PasswordHash(string password)
		{
			byte[] salt;
			new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);

			var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 1000);
			byte[] hash = pbkdf2.GetBytes(20);

			byte[] hashBytes = new byte[36];
			Array.Copy(salt, 0, hashBytes, 0, 16);
			Array.Copy(hash, 0, hashBytes, 16, 20);

			return Convert.ToBase64String(hashBytes);
		}

		private bool ValidatePasswordHash(string password, string dbPassword)
		{
			byte[] hashBytes = Convert.FromBase64String(dbPassword);

			byte[] salt = new byte[16];
			Array.Copy(hashBytes, 0, salt, 0, 16);

			var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 1000);
			byte[] hash = pbkdf2.GetBytes(20);

			for (int i = 0; i < 20; i++)
			{
				if (hashBytes[i + 16] != hash[i])
				{
					return false;
				}
			}

			return true;
		}
		//, List<mainUserRoles> roles
		private string GetJWTAuthKey(mainUsers user,List<mainRols> roles)
		{
			var securtityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_tokenSettings.Key));

			var credentials = new SigningCredentials(securtityKey, SecurityAlgorithms.HmacSha256);

			var claims = new List<Claim>();
			claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
			//claims.Add(new Claim("LastName", user.LastName));
			if ((roles?.Count ?? 0) > 0)
			{
				foreach (var role in roles)
				{
					claims.Add(new Claim(ClaimTypes.Role, role.ResourceName));
				}
			}

			var jwtSecurityToken = new JwtSecurityToken(
				issuer: _tokenSettings.Issuer,
				audience: _tokenSettings.Audience,
				expires: DateTime.Now.AddMinutes(30),
				signingCredentials: credentials,
				claims: claims
			);

			return new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);
		}

		public string Login(LoginInputType loginInput)
		{
			if (string.IsNullOrEmpty(loginInput.Login)
			|| string.IsNullOrEmpty(loginInput.Passowrd))
			{
				return "Invalid Credentials";
			}

			var user = _authContext.mainUsers.Where(_ => _.Login == loginInput.Login).FirstOrDefault();
			if (user == null)
			{
				return "Invalid Credentials";
			}

			if (!ValidatePasswordHash(loginInput.Passowrd, user.Password))
			{
				return "Invalid Credentials";
			}

			//var roles = _authContext.UserRoles.Where(_ => _.UserId == user.UserId).ToList();
			var rols = user.Rol;
			var permission = _authContext.mainRols.Where(_ => _.Id == rols).ToList();
			return GetJWTAuthKey(user, permission);
		}

	}
}

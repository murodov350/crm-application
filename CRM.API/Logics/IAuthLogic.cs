﻿using CRM.API.InputTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM.API.Logics
{
    public interface IAuthLogic
    {
        string Register(RegisterInputType registerInput);
        string Login(LoginInputType loginInput);
    }
}

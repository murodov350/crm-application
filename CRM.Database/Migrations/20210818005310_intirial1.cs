﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CRM.Database.Migrations
{
    public partial class intirial1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_mainReturnPurchase_mainWarehouse_CounterpartyId",
                table: "mainReturnPurchase");

            migrationBuilder.DropIndex(
                name: "IX_mainReturnPurchase_CounterpartyId",
                table: "mainReturnPurchase");

            migrationBuilder.DropColumn(
                name: "Showcase",
                table: "mainSale");

            migrationBuilder.DropColumn(
                name: "CounterpartyId",
                table: "mainReturnPurchase");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Updated",
                table: "mainSale",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Status",
                table: "mainSale",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "PriceUZS",
                table: "mainSale",
                type: "float",
                nullable: false,
                defaultValue: 0.0,
                oldClrType: typeof(double),
                oldType: "float",
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "PriceUSD",
                table: "mainSale",
                type: "float",
                nullable: false,
                defaultValue: 0.0,
                oldClrType: typeof(double),
                oldType: "float",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "Inserted",
                table: "mainSale",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "Discount",
                table: "mainSale",
                type: "float",
                nullable: false,
                defaultValue: 0.0,
                oldClrType: typeof(double),
                oldType: "float",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Deleted",
                table: "mainSale",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateSale",
                table: "mainSale",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ShowcaseId",
                table: "mainSale",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Ammount",
                table: "mainOrder",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CounterpartyId",
                table: "mainOrder",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateOrder",
                table: "mainOrder",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Document",
                table: "mainOrder",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ProductId",
                table: "mainOrder",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "Inserted",
                table: "mainFactory",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "mainCounterparty",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_mainUsers_Rol",
                table: "mainUsers",
                column: "Rol");

            migrationBuilder.CreateIndex(
                name: "IX_mainSale_ShowcaseId",
                table: "mainSale",
                column: "ShowcaseId");

            migrationBuilder.CreateIndex(
                name: "IX_mainReturnPurchase_WarehouseId",
                table: "mainReturnPurchase",
                column: "WarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_mainOrder_CounterpartyId",
                table: "mainOrder",
                column: "CounterpartyId");

            migrationBuilder.CreateIndex(
                name: "IX_mainOrder_ProductId",
                table: "mainOrder",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_mainOrder_mainCounterparty_CounterpartyId",
                table: "mainOrder",
                column: "CounterpartyId",
                principalTable: "mainCounterparty",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_mainOrder_mainProduct_ProductId",
                table: "mainOrder",
                column: "ProductId",
                principalTable: "mainProduct",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_mainReturnPurchase_mainWarehouse_WarehouseId",
                table: "mainReturnPurchase",
                column: "WarehouseId",
                principalTable: "mainWarehouse",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_mainSale_mainShowcase_ShowcaseId",
                table: "mainSale",
                column: "ShowcaseId",
                principalTable: "mainShowcase",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_mainUsers_mainRols_Rol",
                table: "mainUsers",
                column: "Rol",
                principalTable: "mainRols",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_mainOrder_mainCounterparty_CounterpartyId",
                table: "mainOrder");

            migrationBuilder.DropForeignKey(
                name: "FK_mainOrder_mainProduct_ProductId",
                table: "mainOrder");

            migrationBuilder.DropForeignKey(
                name: "FK_mainReturnPurchase_mainWarehouse_WarehouseId",
                table: "mainReturnPurchase");

            migrationBuilder.DropForeignKey(
                name: "FK_mainSale_mainShowcase_ShowcaseId",
                table: "mainSale");

            migrationBuilder.DropForeignKey(
                name: "FK_mainUsers_mainRols_Rol",
                table: "mainUsers");

            migrationBuilder.DropIndex(
                name: "IX_mainUsers_Rol",
                table: "mainUsers");

            migrationBuilder.DropIndex(
                name: "IX_mainSale_ShowcaseId",
                table: "mainSale");

            migrationBuilder.DropIndex(
                name: "IX_mainReturnPurchase_WarehouseId",
                table: "mainReturnPurchase");

            migrationBuilder.DropIndex(
                name: "IX_mainOrder_CounterpartyId",
                table: "mainOrder");

            migrationBuilder.DropIndex(
                name: "IX_mainOrder_ProductId",
                table: "mainOrder");

            migrationBuilder.DropColumn(
                name: "ShowcaseId",
                table: "mainSale");

            migrationBuilder.DropColumn(
                name: "Ammount",
                table: "mainOrder");

            migrationBuilder.DropColumn(
                name: "CounterpartyId",
                table: "mainOrder");

            migrationBuilder.DropColumn(
                name: "DateOrder",
                table: "mainOrder");

            migrationBuilder.DropColumn(
                name: "Document",
                table: "mainOrder");

            migrationBuilder.DropColumn(
                name: "ProductId",
                table: "mainOrder");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "mainCounterparty");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Updated",
                table: "mainSale",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<int>(
                name: "Status",
                table: "mainSale",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<double>(
                name: "PriceUZS",
                table: "mainSale",
                type: "float",
                nullable: true,
                oldClrType: typeof(double),
                oldType: "float");

            migrationBuilder.AlterColumn<double>(
                name: "PriceUSD",
                table: "mainSale",
                type: "float",
                nullable: true,
                oldClrType: typeof(double),
                oldType: "float");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Inserted",
                table: "mainSale",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<double>(
                name: "Discount",
                table: "mainSale",
                type: "float",
                nullable: true,
                oldClrType: typeof(double),
                oldType: "float");

            migrationBuilder.AlterColumn<int>(
                name: "Deleted",
                table: "mainSale",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateSale",
                table: "mainSale",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<bool>(
                name: "Showcase",
                table: "mainSale",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CounterpartyId",
                table: "mainReturnPurchase",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "Inserted",
                table: "mainFactory",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_mainReturnPurchase_CounterpartyId",
                table: "mainReturnPurchase",
                column: "CounterpartyId");

            migrationBuilder.AddForeignKey(
                name: "FK_mainReturnPurchase_mainWarehouse_CounterpartyId",
                table: "mainReturnPurchase",
                column: "CounterpartyId",
                principalTable: "mainWarehouse",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

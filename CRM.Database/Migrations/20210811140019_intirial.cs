﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CRM.Database.Migrations
{
    public partial class intirial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "mainCounterparty",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Lastname = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Middlename = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true),
                    Deleted = table.Column<int>(type: "int", nullable: true),
                    Inserted = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mainCounterparty", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "mainFactory",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true),
                    Deleted = table.Column<int>(type: "int", nullable: true),
                    Inserted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mainFactory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "mainManager",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Lastname = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Middlename = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Deleted = table.Column<int>(type: "int", nullable: false),
                    Inserted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mainManager", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "mainOrder",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Deleted = table.Column<int>(type: "int", nullable: false),
                    Inserted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mainOrder", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "mainPermission",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ResourceName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Comment = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mainPermission", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "mainRols",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ParentId = table.Column<int>(type: "int", nullable: false),
                    ResourceName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mainRols", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "mainSection",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Deleted = table.Column<int>(type: "int", nullable: false),
                    Inserted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mainSection", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "mainWarehouse",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Deleted = table.Column<int>(type: "int", nullable: false),
                    Inserted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mainWarehouse", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "moneyPaymentType",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Deleted = table.Column<int>(type: "int", nullable: false),
                    Inserted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_moneyPaymentType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "moneyPurposePyment",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Deleted = table.Column<int>(type: "int", nullable: false),
                    Inserted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_moneyPurposePyment", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "productCategory",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Deleted = table.Column<int>(type: "int", nullable: false),
                    Inserted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_productCategory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "productUnit",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Deleted = table.Column<int>(type: "int", nullable: false),
                    Inserted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_productUnit", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "mainManagerCounterparty",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ManagerId = table.Column<int>(type: "int", nullable: true),
                    CounterpartyId = table.Column<int>(type: "int", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Deleted = table.Column<int>(type: "int", nullable: false),
                    Inserted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mainManagerCounterparty", x => x.Id);
                    table.ForeignKey(
                        name: "FK_mainManagerCounterparty_mainCounterparty_CounterpartyId",
                        column: x => x.CounterpartyId,
                        principalTable: "mainCounterparty",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_mainManagerCounterparty_mainManager_ManagerId",
                        column: x => x.ManagerId,
                        principalTable: "mainManager",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "mainUsers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ManagerId = table.Column<int>(type: "int", nullable: true),
                    CounterpartyId = table.Column<int>(type: "int", nullable: true),
                    Login = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Rol = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Deleted = table.Column<int>(type: "int", nullable: false),
                    Inserted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mainUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_mainUsers_mainCounterparty_CounterpartyId",
                        column: x => x.CounterpartyId,
                        principalTable: "mainCounterparty",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_mainUsers_mainManager_ManagerId",
                        column: x => x.ManagerId,
                        principalTable: "mainManager",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "mainOrderComment",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ManagerId = table.Column<int>(type: "int", nullable: true),
                    OrderId = table.Column<int>(type: "int", nullable: true),
                    CommentsText = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Deleted = table.Column<int>(type: "int", nullable: false),
                    Inserted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mainOrderComment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_mainOrderComment_mainManager_ManagerId",
                        column: x => x.ManagerId,
                        principalTable: "mainManager",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_mainOrderComment_mainOrder_OrderId",
                        column: x => x.OrderId,
                        principalTable: "mainOrder",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "mainRolesPermissions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RolsId = table.Column<int>(type: "int", nullable: false),
                    PermissionId = table.Column<int>(type: "int", nullable: false),
                    View = table.Column<bool>(type: "bit", nullable: false),
                    Create = table.Column<bool>(type: "bit", nullable: false),
                    Edit = table.Column<bool>(type: "bit", nullable: false),
                    Delete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mainRolesPermissions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_mainRolesPermissions_mainPermission_PermissionId",
                        column: x => x.PermissionId,
                        principalTable: "mainPermission",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_mainRolesPermissions_mainRols_RolsId",
                        column: x => x.RolsId,
                        principalTable: "mainRols",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "mainManagerRole",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ManagerId = table.Column<int>(type: "int", nullable: true),
                    SectionId = table.Column<int>(type: "int", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Deleted = table.Column<int>(type: "int", nullable: false),
                    Inserted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mainManagerRole", x => x.Id);
                    table.ForeignKey(
                        name: "FK_mainManagerRole_mainManager_ManagerId",
                        column: x => x.ManagerId,
                        principalTable: "mainManager",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_mainManagerRole_mainSection_SectionId",
                        column: x => x.SectionId,
                        principalTable: "mainSection",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "mainProduct",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NameModel = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NameParty = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Article = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CategoryId = table.Column<int>(type: "int", nullable: true),
                    UnitId = table.Column<int>(type: "int", nullable: true),
                    PurchaseUSD = table.Column<double>(type: "float", nullable: false),
                    CostPriceUSD = table.Column<double>(type: "float", nullable: false),
                    SalePriceUSD = table.Column<double>(type: "float", nullable: false),
                    Discount = table.Column<double>(type: "float", nullable: false),
                    Photo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Deleted = table.Column<int>(type: "int", nullable: false),
                    Inserted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mainProduct", x => x.Id);
                    table.ForeignKey(
                        name: "FK_mainProduct_productCategory_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "productCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_mainProduct_productUnit_UnitId",
                        column: x => x.UnitId,
                        principalTable: "productUnit",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "mainMoney",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<int>(type: "int", nullable: false),
                    DateMoney = table.Column<DateTime>(type: "datetime2", nullable: false),
                    WarehouseId = table.Column<int>(type: "int", nullable: true),
                    CounterpartyId = table.Column<int>(type: "int", nullable: true),
                    PurposePymentId = table.Column<int>(type: "int", nullable: true),
                    ExchangeRate = table.Column<double>(type: "float", nullable: false),
                    AmmountUSD = table.Column<double>(type: "float", nullable: false),
                    PaymentTypeId = table.Column<int>(type: "int", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Deleted = table.Column<int>(type: "int", nullable: false),
                    Inserted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mainMoney", x => x.Id);
                    table.ForeignKey(
                        name: "FK_mainMoney_mainCounterparty_CounterpartyId",
                        column: x => x.CounterpartyId,
                        principalTable: "mainCounterparty",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_mainMoney_mainUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "mainUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_mainMoney_mainWarehouse_WarehouseId",
                        column: x => x.WarehouseId,
                        principalTable: "mainWarehouse",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_mainMoney_moneyPaymentType_PaymentTypeId",
                        column: x => x.PaymentTypeId,
                        principalTable: "moneyPaymentType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_mainMoney_moneyPurposePyment_PurposePymentId",
                        column: x => x.PurposePymentId,
                        principalTable: "moneyPurposePyment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "mainReturnSale",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateReturnSale = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Document = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    UsersId = table.Column<int>(type: "int", nullable: true),
                    FactoryId = table.Column<int>(type: "int", nullable: true),
                    WarehouseId = table.Column<int>(type: "int", nullable: true),
                    Amount = table.Column<int>(type: "int", nullable: false),
                    PriceUZS = table.Column<double>(type: "float", nullable: false),
                    PriceUSD = table.Column<double>(type: "float", nullable: false),
                    Deleted = table.Column<int>(type: "int", nullable: false),
                    Inserted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mainReturnSale", x => x.Id);
                    table.ForeignKey(
                        name: "FK_mainReturnSale_mainFactory_FactoryId",
                        column: x => x.FactoryId,
                        principalTable: "mainFactory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_mainReturnSale_mainUsers_UsersId",
                        column: x => x.UsersId,
                        principalTable: "mainUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_mainReturnSale_mainWarehouse_WarehouseId",
                        column: x => x.WarehouseId,
                        principalTable: "mainWarehouse",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "mainSale",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateSale = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Document = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UsersId = table.Column<int>(type: "int", nullable: true),
                    WarehouseId = table.Column<int>(type: "int", nullable: true),
                    CounterpartyId = table.Column<int>(type: "int", nullable: true),
                    Amount = table.Column<int>(type: "int", nullable: true),
                    PriceUZS = table.Column<double>(type: "float", nullable: true),
                    PriceUSD = table.Column<double>(type: "float", nullable: true),
                    Discount = table.Column<double>(type: "float", nullable: true),
                    Showcase = table.Column<bool>(type: "bit", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true),
                    Deleted = table.Column<int>(type: "int", nullable: true),
                    Inserted = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mainSale", x => x.Id);
                    table.ForeignKey(
                        name: "FK_mainSale_mainCounterparty_CounterpartyId",
                        column: x => x.CounterpartyId,
                        principalTable: "mainCounterparty",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_mainSale_mainUsers_UsersId",
                        column: x => x.UsersId,
                        principalTable: "mainUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_mainSale_mainWarehouse_WarehouseId",
                        column: x => x.WarehouseId,
                        principalTable: "mainWarehouse",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "mainPurchase",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductId = table.Column<int>(type: "int", nullable: true),
                    DatePurchase = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Document = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UsersId = table.Column<int>(type: "int", nullable: true),
                    CounterpartyId = table.Column<int>(type: "int", nullable: true),
                    FactoryId = table.Column<int>(type: "int", nullable: true),
                    Amount = table.Column<int>(type: "int", nullable: false),
                    PriceUSD = table.Column<double>(type: "float", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Deleted = table.Column<int>(type: "int", nullable: false),
                    Inserted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mainPurchase", x => x.Id);
                    table.ForeignKey(
                        name: "FK_mainPurchase_mainCounterparty_CounterpartyId",
                        column: x => x.CounterpartyId,
                        principalTable: "mainCounterparty",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_mainPurchase_mainFactory_FactoryId",
                        column: x => x.FactoryId,
                        principalTable: "mainFactory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_mainPurchase_mainProduct_ProductId",
                        column: x => x.ProductId,
                        principalTable: "mainProduct",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_mainPurchase_mainUsers_UsersId",
                        column: x => x.UsersId,
                        principalTable: "mainUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "mainShowcase",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductId = table.Column<int>(type: "int", nullable: true),
                    CounterpartyId = table.Column<int>(type: "int", nullable: true),
                    Article = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Count = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Deleted = table.Column<int>(type: "int", nullable: false),
                    Inserted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mainShowcase", x => x.Id);
                    table.ForeignKey(
                        name: "FK_mainShowcase_mainCounterparty_CounterpartyId",
                        column: x => x.CounterpartyId,
                        principalTable: "mainCounterparty",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_mainShowcase_mainProduct_ProductId",
                        column: x => x.ProductId,
                        principalTable: "mainProduct",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "mainMoneyComment",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CounterpartyId = table.Column<int>(type: "int", nullable: true),
                    MoneyId = table.Column<int>(type: "int", nullable: true),
                    CommentsText = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Deleted = table.Column<int>(type: "int", nullable: false),
                    Inserted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mainMoneyComment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_mainMoneyComment_mainCounterparty_CounterpartyId",
                        column: x => x.CounterpartyId,
                        principalTable: "mainCounterparty",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_mainMoneyComment_mainMoney_MoneyId",
                        column: x => x.MoneyId,
                        principalTable: "mainMoney",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "mainReturnPurchase",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PurchaseId = table.Column<int>(type: "int", nullable: true),
                    DateReturnPurchase = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Document = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    UsersId = table.Column<int>(type: "int", nullable: true),
                    WarehouseId = table.Column<int>(type: "int", nullable: true),
                    FactoryId = table.Column<int>(type: "int", nullable: true),
                    Amount = table.Column<int>(type: "int", nullable: false),
                    PriceUSD = table.Column<double>(type: "float", nullable: false),
                    Deleted = table.Column<int>(type: "int", nullable: false),
                    Inserted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Updated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CounterpartyId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mainReturnPurchase", x => x.Id);
                    table.ForeignKey(
                        name: "FK_mainReturnPurchase_mainFactory_FactoryId",
                        column: x => x.FactoryId,
                        principalTable: "mainFactory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_mainReturnPurchase_mainPurchase_PurchaseId",
                        column: x => x.PurchaseId,
                        principalTable: "mainPurchase",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_mainReturnPurchase_mainUsers_UsersId",
                        column: x => x.UsersId,
                        principalTable: "mainUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_mainReturnPurchase_mainWarehouse_CounterpartyId",
                        column: x => x.CounterpartyId,
                        principalTable: "mainWarehouse",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_mainManagerCounterparty_CounterpartyId",
                table: "mainManagerCounterparty",
                column: "CounterpartyId");

            migrationBuilder.CreateIndex(
                name: "IX_mainManagerCounterparty_ManagerId",
                table: "mainManagerCounterparty",
                column: "ManagerId");

            migrationBuilder.CreateIndex(
                name: "IX_mainManagerRole_ManagerId",
                table: "mainManagerRole",
                column: "ManagerId");

            migrationBuilder.CreateIndex(
                name: "IX_mainManagerRole_SectionId",
                table: "mainManagerRole",
                column: "SectionId");

            migrationBuilder.CreateIndex(
                name: "IX_mainMoney_CounterpartyId",
                table: "mainMoney",
                column: "CounterpartyId");

            migrationBuilder.CreateIndex(
                name: "IX_mainMoney_PaymentTypeId",
                table: "mainMoney",
                column: "PaymentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_mainMoney_PurposePymentId",
                table: "mainMoney",
                column: "PurposePymentId");

            migrationBuilder.CreateIndex(
                name: "IX_mainMoney_UserId",
                table: "mainMoney",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_mainMoney_WarehouseId",
                table: "mainMoney",
                column: "WarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_mainMoneyComment_CounterpartyId",
                table: "mainMoneyComment",
                column: "CounterpartyId");

            migrationBuilder.CreateIndex(
                name: "IX_mainMoneyComment_MoneyId",
                table: "mainMoneyComment",
                column: "MoneyId");

            migrationBuilder.CreateIndex(
                name: "IX_mainOrderComment_ManagerId",
                table: "mainOrderComment",
                column: "ManagerId");

            migrationBuilder.CreateIndex(
                name: "IX_mainOrderComment_OrderId",
                table: "mainOrderComment",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_mainProduct_CategoryId",
                table: "mainProduct",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_mainProduct_UnitId",
                table: "mainProduct",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_mainPurchase_CounterpartyId",
                table: "mainPurchase",
                column: "CounterpartyId");

            migrationBuilder.CreateIndex(
                name: "IX_mainPurchase_FactoryId",
                table: "mainPurchase",
                column: "FactoryId");

            migrationBuilder.CreateIndex(
                name: "IX_mainPurchase_ProductId",
                table: "mainPurchase",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_mainPurchase_UsersId",
                table: "mainPurchase",
                column: "UsersId");

            migrationBuilder.CreateIndex(
                name: "IX_mainReturnPurchase_CounterpartyId",
                table: "mainReturnPurchase",
                column: "CounterpartyId");

            migrationBuilder.CreateIndex(
                name: "IX_mainReturnPurchase_FactoryId",
                table: "mainReturnPurchase",
                column: "FactoryId");

            migrationBuilder.CreateIndex(
                name: "IX_mainReturnPurchase_PurchaseId",
                table: "mainReturnPurchase",
                column: "PurchaseId");

            migrationBuilder.CreateIndex(
                name: "IX_mainReturnPurchase_UsersId",
                table: "mainReturnPurchase",
                column: "UsersId");

            migrationBuilder.CreateIndex(
                name: "IX_mainReturnSale_FactoryId",
                table: "mainReturnSale",
                column: "FactoryId");

            migrationBuilder.CreateIndex(
                name: "IX_mainReturnSale_UsersId",
                table: "mainReturnSale",
                column: "UsersId");

            migrationBuilder.CreateIndex(
                name: "IX_mainReturnSale_WarehouseId",
                table: "mainReturnSale",
                column: "WarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_mainRolesPermissions_PermissionId",
                table: "mainRolesPermissions",
                column: "PermissionId");

            migrationBuilder.CreateIndex(
                name: "IX_mainRolesPermissions_RolsId",
                table: "mainRolesPermissions",
                column: "RolsId");

            migrationBuilder.CreateIndex(
                name: "IX_mainSale_CounterpartyId",
                table: "mainSale",
                column: "CounterpartyId");

            migrationBuilder.CreateIndex(
                name: "IX_mainSale_UsersId",
                table: "mainSale",
                column: "UsersId");

            migrationBuilder.CreateIndex(
                name: "IX_mainSale_WarehouseId",
                table: "mainSale",
                column: "WarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_mainShowcase_CounterpartyId",
                table: "mainShowcase",
                column: "CounterpartyId");

            migrationBuilder.CreateIndex(
                name: "IX_mainShowcase_ProductId",
                table: "mainShowcase",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_mainUsers_CounterpartyId",
                table: "mainUsers",
                column: "CounterpartyId");

            migrationBuilder.CreateIndex(
                name: "IX_mainUsers_ManagerId",
                table: "mainUsers",
                column: "ManagerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "mainManagerCounterparty");

            migrationBuilder.DropTable(
                name: "mainManagerRole");

            migrationBuilder.DropTable(
                name: "mainMoneyComment");

            migrationBuilder.DropTable(
                name: "mainOrderComment");

            migrationBuilder.DropTable(
                name: "mainReturnPurchase");

            migrationBuilder.DropTable(
                name: "mainReturnSale");

            migrationBuilder.DropTable(
                name: "mainRolesPermissions");

            migrationBuilder.DropTable(
                name: "mainSale");

            migrationBuilder.DropTable(
                name: "mainShowcase");

            migrationBuilder.DropTable(
                name: "mainSection");

            migrationBuilder.DropTable(
                name: "mainMoney");

            migrationBuilder.DropTable(
                name: "mainOrder");

            migrationBuilder.DropTable(
                name: "mainPurchase");

            migrationBuilder.DropTable(
                name: "mainPermission");

            migrationBuilder.DropTable(
                name: "mainRols");

            migrationBuilder.DropTable(
                name: "mainWarehouse");

            migrationBuilder.DropTable(
                name: "moneyPaymentType");

            migrationBuilder.DropTable(
                name: "moneyPurposePyment");

            migrationBuilder.DropTable(
                name: "mainFactory");

            migrationBuilder.DropTable(
                name: "mainProduct");

            migrationBuilder.DropTable(
                name: "mainUsers");

            migrationBuilder.DropTable(
                name: "productCategory");

            migrationBuilder.DropTable(
                name: "productUnit");

            migrationBuilder.DropTable(
                name: "mainCounterparty");

            migrationBuilder.DropTable(
                name: "mainManager");
        }
    }
}

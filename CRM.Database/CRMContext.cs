﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using CRM.Database.Models;

namespace CRM.Database
{
    public class CRMContext : DbContext
    {
        public CRMContext(DbContextOptions<CRMContext> options)
           : base(options)
        {
        }
        public CRMContext()
        {

        }
        public DbSet<mainCounterparty> mainCounterparty { get; set; }
        public DbSet<mainFactory> mainFactory { get; set; }
        //public DbSet<mainLeftover> Leftovers { get; set; }
        public DbSet<mainManager> mainManager { get; set; }
        public DbSet<mainSection> mainSection { get; set; }
        public DbSet<mainManagerCounterparty> mainManagerCounterparty { get; set; }
        public DbSet<mainManagerRole> mainManagerRole { get; set; }
        public DbSet<mainMoney> mainMoney { get; set; }
        public DbSet<mainMoneyComment> mainMoneyComment { get; set; }
        public DbSet<mainOrder> mainOrder { get; set; }
        public DbSet<mainPermission> mainPermission { get; set; }
        public DbSet<mainProduct> mainProduct { get; set; }
        public DbSet<mainPurchase> mainPurchase { get; set; }
        public DbSet<mainReturnPurchase> mainReturnPurchase { get; set; }
        public DbSet<mainReturnSale> mainReturnSale { get; set; }
        public DbSet<mainRolesPermissions> mainRolesPermissions { get; set; }
        public DbSet<mainRols> mainRols { get; set; }
        public DbSet<mainShowcase> mainShowcase { get; set; }
        public DbSet<mainUsers> mainUsers { get; set; }
        public DbSet<mainWarehouse> mainWarehouse { get; set; }
        public DbSet<moneyPaymentType> moneyPaymentType { get; set; }
        public DbSet<moneyPurposePyment> moneyPurposePyment { get; set; }
        public DbSet<productCategory> productCategory { get; set; }
        public DbSet<productUnit> productUnit { get; set; }
        public DbSet<mainSale> mainSale { get; set; }
        public DbSet<mainOrderComment> mainOrderComment { get; set; }

        //public DbSet<Property> Properties { get; set; }
        //public DbSet<Payment> Payments { get; set; }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Database.Models
{
    public class mainRols
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string ResourceName { get; set; }
        public bool Status { get; set; }

        public virtual ICollection<mainRolesPermissions> mainRolesPermissions { get; set; }
        public virtual ICollection<mainUsers> mainUsers { get; set; }

    }
    public class rolsInput {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string ResourceName { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CRM.Database.Models
{
    [Table("mainManager")]
    public class mainManager
    {
        public int Id { get; set; }
        public string Lastname { get; set; }
        public string Name { get; set; }
        public string Middlename { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public int Status { get; set; }
        public int Deleted { get; set; }
        public DateTime Inserted { get; set; }
        public DateTime Updated { get; set; }

        public virtual ICollection<mainManagerCounterparty> mainManagerCounterparty { get; set; }
        public virtual ICollection<mainManagerRole> mainManagerRole { get; set; }
        public virtual ICollection<mainOrderComment> mainOrderComment { get; set; }
        public virtual ICollection<mainUsers> mainUsers { get; set; }


    }
    public class managerInput
    {
        public int? Id { get; set; }
        public string Lastname { get; set; }
        public string Name { get; set; }
        public string Middlename { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CRM.Database.Models
{
    [Table("mainFactory")]
    public class mainFactory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public int? Status { get; set; }
        public int? Deleted { get; set; }
        public DateTime? Inserted { get; set; }
        public DateTime? Updated { get; set; }

        public virtual ICollection<mainPurchase> mainPurchase { get; set; }
        public virtual ICollection<mainReturnPurchase> mainReturnPurchase { get; set; }
        public virtual ICollection<mainReturnSale> mainReturnSale { get; set; }


    }
    public class factoryInput
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CRM.Database.Models
{
    [Table("mainSale")]
    public class mainSale
    {
        [Key]
        public int Id { get; set; }
        public DateTime DateSale { get; set; }
        public string Document { get; set; }
        public int? UsersId { get; set; }
        public int? WarehouseId { get; set; }
        public int? CounterpartyId { get; set; }
        public int? Amount { get; set; }
        public double PriceUZS { get; set; }
        public double PriceUSD { get; set; }
        public double Discount { get; set; }
        public int ShowcaseId { get; set; }
        public int Status { get; set; }
        public int Deleted { get; set; }
        public DateTime Inserted { get; set; }
        public DateTime Updated { get; set; }

        [ForeignKey("ShowcaseId")]
        public virtual mainShowcase mainShowcase { get; set; }

        [ForeignKey("WarehouseId")]
        public virtual mainWarehouse mainWarehouse { get; set; }
        [ForeignKey("UsersId")]
        public virtual mainUsers mainUsers { get; set; }
        [ForeignKey("CounterpartyId")]
        public virtual mainCounterparty mainCounterparty { get; set; }

    }
    public class saleInput
    {
        public int? Id { get; set; }
        public DateTime DateSale { get; set; }
        public string Document { get; set; }
        public int UsersId { get; set; }
        public int WarehouseId { get; set; }
        public int CounterpartyId { get; set; }
        public int Amount { get; set; }
        public double PriceUZS { get; set; }
        public double PriceUSD { get; set; }
        public double Discount { get; set; }
        public int ShowcaseId { get; set; }
    }
}

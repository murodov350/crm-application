﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CRM.Database.Models
{
    public class mainShowcase
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public int? CounterpartyId { get; set; }
        public string Article { get; set; }
        public int Count { get; set; }
        
        public int Status { get; set; }
        public int Deleted { get; set; }
        public DateTime Inserted { get; set; }
        public DateTime Updated { get; set; }


        [ForeignKey("ProductId")]
        public virtual mainProduct mainProduct { get; set; }
        [ForeignKey("CounterpartyId")]
        public virtual mainCounterparty mainCounterparty { get; set; }

        public ICollection<mainSale> mainSales { get; set; }

    }
    public class showcaseInput
    {
        public int? Id { get; set; }
        public int ProductId { get; set; }
        public int CounterpartyId { get; set; }
        public string Article { get; set; }
        public int Count { get; set; }
    }
}

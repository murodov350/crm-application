﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CRM.Database.Models
{
    public class mainProduct
    {
        public int Id { get; set; }
        public string NameModel { get; set; }
        public string NameParty { get; set; }
        public string Article { get; set; }
        public int? CategoryId { get; set; }
        public int? UnitId { get; set; }
        public double PurchaseUSD { get; set; }
        public double CostPriceUSD { get; set; }
        public double SalePriceUSD { get; set; }
        public double Discount { get; set; }
        public string Photo { get; set; }
        public int Status { get; set; }
        public int Deleted { get; set; }
        public DateTime Inserted { get; set; }
        public DateTime Updated { get; set; }

        [ForeignKey("CategoryId")]
        public virtual productCategory productCategory { get; set; }
        [ForeignKey("UnitId")]
        public virtual productUnit productUnit { get; set; }

        public virtual ICollection<mainPurchase> mainPurchase { get; set; }
        public virtual ICollection<mainShowcase> mainShowcase { get; set; }
        public virtual ICollection<mainOrder> mainOrder { get; set; }

    }
    public class productInput
    {
        public int? Id { get; set; }
        public string NameModel { get; set; }
        public string NameParty { get; set; }
        public string Article { get; set; }
        public int CategoryId { get; set; }
        public int UnitId { get; set; }
        public double PurchaseUSD { get; set; }
        public double CostPriceUSD { get; set; }
        public double SalePriceUSD { get; set; }
        public double Discount { get; set; }
        public string Photo { get; set; }
    }

}

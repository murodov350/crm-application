﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CRM.Database.Models
{
    public class mainRolesPermissions
    {
        public int Id { get; set; }
        public int RolsId { get; set; }
        public int PermissionId { get; set; }

        public bool View { get; set; }

        public bool Create { get; set; }

        public bool Edit { get; set; }

        public bool Delete { get; set; }


        [ForeignKey("RolsId")]
        public virtual mainRols mainRols { get; set; }
        [ForeignKey("PermissionId")]
        public virtual mainPermission mainPermission { get; set; }

    }
    public class rolesPermissionsInput
    {
        public int? Id { get; set; }
        public int RolsId { get; set; }
        public int PermissionId { get; set; }

        public bool View { get; set; }

        public bool Create { get; set; }

        public bool Edit { get; set; }

        public bool Delete { get; set; }
    }
}

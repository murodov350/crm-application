﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CRM.Database.Models
{
    [Table("mainOrder")]
    public class mainOrder
    {
        public int Id { get; set; }
        public DateTime DateOrder { get; set; }
        public int? CounterpartyId { get; set; }
        public int? ProductId { get; set; }
        public string Document { get; set; }
        public int Ammount { get; set; }
        public int Status { get; set; }
        public int Deleted { get; set; }
        public DateTime Inserted { get; set; }
        public DateTime Updated { get; set; }

        [ForeignKey("CounterpartyId")]
        public virtual mainCounterparty mainCounterparty { get; set; }
        [ForeignKey("ProductId")]
        public virtual mainProduct mainProduct { get; set; }

        public virtual ICollection<mainOrderComment> mainOrderComment { get; set; }

    }
    public class orderInput
    {
        public int? Id { get; set; }
        public DateTime DateOrder { get; set; }
        public int? CounterpartyId { get; set; }
        public int? ProductId { get; set; }
        public string Document { get; set; }
        public int Ammount { get; set; }
    }
}

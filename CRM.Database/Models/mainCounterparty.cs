﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Database.Models
{
    [Table("mainCounterparty")]
    public class mainCounterparty
    {
        [Key]
        public int Id { get; set; }
        public string Lastname { get; set; }
        public string Name { get; set; }
        public string Middlename { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public int? Status { get; set; }
        public int? Deleted { get; set; }
        public DateTime? Inserted { get; set; }
        public DateTime? Updated { get; set; }


        public virtual ICollection<mainManagerCounterparty> mainManagerCounterparty { get; set; }
        public virtual ICollection<mainMoney> mainMoney { get; set; }
        public virtual ICollection<mainMoneyComment> mainMoneyComment { get; set; }
        public virtual ICollection<mainPurchase> mainPurchase { get; set; }
        public virtual ICollection<mainSale> mainSale { get; set; }
        public virtual ICollection<mainShowcase> mainShowcase { get; set; }
        public virtual ICollection<mainUsers> mainUsers { get; set; }
        public virtual ICollection<mainOrder> mainOrder { get; set; }
    }
    public class counterpartyInput
    {
        public int? Id { get; set; }
        public string Lastname { get; set; }
        public string Name { get; set; }
        public string Middlename { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

    }
}

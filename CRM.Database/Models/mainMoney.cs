﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CRM.Database.Models
{
    [Table("mainMoney")]

    public class mainMoney
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public DateTime DateMoney { get; set; }
        public int? WarehouseId { get; set; }
        public int? CounterpartyId { get; set; }
        public int? PurposePymentId { get; set; }
        public double ExchangeRate { get; set; }
        public double AmmountUSD { get; set; }
        public int? PaymentTypeId { get; set; }
        public int? UserId { get; set; }
        public int Status { get; set; }
        public int Deleted { get; set; }
        public DateTime Inserted { get; set; }
        public DateTime Updated { get; set; }

        [ForeignKey("WarehouseId")]
        public virtual mainWarehouse mainWarehouse { get; set; }
        [ForeignKey("CounterpartyId")]
        public virtual mainCounterparty mainCounterparty { get; set; }
        [ForeignKey("PurposePymentId")]
        public virtual moneyPurposePyment moneyPurposePyment { get; set; }
        [ForeignKey("PaymentTypeId")]
        public virtual moneyPaymentType moneyPaymentType { get; set; }
        [ForeignKey("UserId")]
        public virtual mainUsers mainUsers { get; set; }

        public virtual ICollection<mainMoneyComment> mainMoneyComment { get; set; }

    }
    public class moneyInput
    {
        public int? Id { get; set; }
        public int Type { get; set; }
        public DateTime DateMoney { get; set; }
        public int WarehouseId { get; set; }
        public int CounterpartyId { get; set; }
        public int PurposePymentId { get; set; }
        public double ExchangeRate { get; set; }
        public double AmmountUSD { get; set; }
        public int PaymentTypeId { get; set; }
        public int UserId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Database.Models
{
    public class productCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public int Deleted { get; set; }
        public DateTime Inserted { get; set; }
        public DateTime Updated { get; set; }

        public virtual ICollection<mainProduct> mainProduct { get; set; }

    }
    public class productCategoryInputs
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}

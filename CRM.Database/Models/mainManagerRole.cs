﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CRM.Database.Models
{
    [Table("mainManagerRole")]
    public class mainManagerRole
    {
        public int Id { get; set; }
        public int? ManagerId { get; set; }
        public int? SectionId { get; set; }

        public int Status { get; set; }
        public int Deleted { get; set; }
        public DateTime Inserted { get; set; }
        public DateTime Updated { get; set; }

        [ForeignKey("ManagerId")]
        public virtual mainManager mainManager { get; set; }
        [ForeignKey("SectionId")]
        public virtual mainSection mainSection { get; set; }
    }
    public class managerRoleInput
    {
        public int? Id { get; set; }
        public int ManagerId { get; set; }
        public int SectionId { get; set; }
    }
}

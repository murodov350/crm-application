﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CRM.Database.Models
{
    public class mainPurchase
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public DateTime DatePurchase { get; set; }
        public string Document { get; set; }
        public int? UsersId { get; set; }
        public int? CounterpartyId { get; set; }
        public int? FactoryId { get; set; }
        public int Amount { get; set; }
        public double PriceUSD { get; set; }
        public int Status { get; set; }
        public int Deleted { get; set; }
        public DateTime Inserted { get; set; }
        public DateTime Updated { get; set; }

        [ForeignKey("ProductId")]
        public virtual mainProduct mainProduct { get; set; }
        [ForeignKey("UsersId")]
        public virtual mainUsers mainUsers { get; set; }
        [ForeignKey("CounterpartyId")]
        public virtual mainCounterparty mainCounterparty { get; set; }
        [ForeignKey("FactoryId")]
        public virtual mainFactory mainFactory { get; set; }

        public virtual ICollection<mainReturnPurchase> mainReturnPurchase { get; set; }

    }
    public class purchaseInput
    {
        public int? Id { get; set; }
        public int ProductId { get; set; }
        public DateTime DatePurchase { get; set; }
        public string Document { get; set; }
        public int UsersId { get; set; }
        public int CounterpartyId { get; set; }
        public int FactoryId { get; set; }
        public int Amount { get; set; }
        public double PriceUSD { get; set; }
    }
}

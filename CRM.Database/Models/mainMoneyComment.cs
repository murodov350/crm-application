﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CRM.Database.Models
{
    [Table("mainMoneyComment")]

    public class mainMoneyComment
    {
        public int Id { get; set; }
        public int? CounterpartyId { get; set; }
        public int? MoneyId { get; set; }
        public string CommentsText { get; set; }
        public int Status { get; set; }
        public int Deleted { get; set; }
        public DateTime Inserted { get; set; }
        public DateTime Updated { get; set; }

        [ForeignKey("CounterpartyId")]
        public virtual mainCounterparty mainCounterparty { get; set; }
        [ForeignKey("MoneyId")]
        public virtual mainMoney mainMoney { get; set; }

    }
    public class moneyCommentInput
    {
        public int? Id { get; set; }
        public int CounterpartyId { get; set; }
        public int MoneyId { get; set; }
        public string CommentsText { get; set; }
    }
}

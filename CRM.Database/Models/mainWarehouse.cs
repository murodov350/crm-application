﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Database.Models
{
    public class mainWarehouse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int Status { get; set; }
        public int Deleted { get; set; }
        public DateTime Inserted { get; set; }
        public DateTime Updated { get; set; }

        public virtual ICollection<mainMoney> mainMoney { get; set; }
        public virtual ICollection<mainReturnPurchase> mainReturnPurchase { get; set; }
        public virtual ICollection<mainReturnSale> mainReturnSale { get; set; }
        public virtual ICollection<mainSale> mainSale { get; set; }



    }
    public class warehouseInput
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
    }
}

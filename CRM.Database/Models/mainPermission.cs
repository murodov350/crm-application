﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRM.Database.Models
{
    public class mainPermission
    {
        public int Id { get; set; }
        public string ResourceName { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }

        public virtual ICollection<mainRolesPermissions> mainRolesPermissions { get; set; }

    }
    public class permissionInput
    {
        public int? Id { get; set; }
        public string ResourceName { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
    }
}

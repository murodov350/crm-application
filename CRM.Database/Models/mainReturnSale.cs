﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CRM.Database.Models
{
    public class mainReturnSale
    {
        public int Id { get; set; }
        public DateTime DateReturnSale { get; set; }
        public string Document { get; set; }
        public int? UsersId { get; set; }
        public int? FactoryId { get; set; }
        public int? WarehouseId { get; set; }
        public int Amount { get; set; }
        public double PriceUZS { get; set; }
        public double PriceUSD { get; set; }
        public int Status { get; set; }
        public int Deleted { get; set; }
        public DateTime Inserted { get; set; }
        public DateTime Updated { get; set; }


        [ForeignKey("UsersId")]
        public virtual mainUsers mainUsers { get; set; }
        [ForeignKey("WarehouseId")]
        public virtual mainWarehouse mainWarehouse { get; set; }
        [ForeignKey("FactoryId")]
        public virtual mainFactory mainFactory { get; set; }
    }
    public class returnSaleInput
    {
        public int? Id { get; set; }
        public DateTime DateReturnSale { get; set; }
        public string Document { get; set; }
        public int UsersId { get; set; }
        public int FactoryId { get; set; }
        public int WarehouseId { get; set; }
        public int Amount { get; set; }
        public double PriceUZS { get; set; }
        public double PriceUSD { get; set; }
    }
}

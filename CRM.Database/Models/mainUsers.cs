﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CRM.Database.Models
{
    public class mainUsers
    {
        public int Id { get; set; }
        public int? ManagerId { get; set; }
        public int? CounterpartyId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public int Rol { get; set; }
        public int Status { get; set; }
        public int Deleted { get; set; }
        public DateTime Inserted { get; set; }
        public DateTime Updated { get; set; }

        [ForeignKey("ManagerId")]
        public virtual mainManager mainManager { get; set; }
        [ForeignKey("CounterpartyId")]
        public virtual mainCounterparty mainCounterparty { get; set; }
        [ForeignKey("Rol")]
        public virtual mainRols mainRols { get; set; }

        public virtual ICollection<mainPurchase> mainPurchase { get; set; }
        public virtual ICollection<mainReturnPurchase> mainReturnPurchase { get; set; }
        public virtual ICollection<mainReturnSale> mainReturnSale { get; set; }
        public virtual ICollection<mainSale> mainSale { get; set; }
        public virtual ICollection<mainMoney> mainMoney { get; set; }

    }
    public class usersInput
    {
        public int? Id { get; set; }
        public int? ManagerId { get; set; }
        public int? CounterpartyId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public int Rol { get; set; }
    }
}

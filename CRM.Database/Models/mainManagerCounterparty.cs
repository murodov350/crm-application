﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CRM.Database.Models
{
    [Table("mainManagerCounterparty")]
    public class mainManagerCounterparty
    {
        [Key]
        public int Id { get; set; }
        public int? ManagerId { get; set; }
        public int? CounterpartyId { get; set; }
        public int Status { get; set; }
        public int Deleted { get; set; }
        public DateTime Inserted { get; set; }
        public DateTime? Updated { get; set; }

        [ForeignKey("ManagerId")]
        public virtual mainManager mainManager { get; set; }
        [ForeignKey("CounterpartyId")]
        public virtual mainCounterparty mainCounterparty { get; set; }
    }
    public class managerCounterpartyInput
    {
        public int? Id { get; set; }
        public int ManagerId { get; set; }
        public int CounterpartyId { get; set; }
    }
}

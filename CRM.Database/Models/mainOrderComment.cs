﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CRM.Database.Models
{
    [Table("mainOrderComment")]
    public class mainOrderComment
    {
        public int Id { get; set; }
        public int? ManagerId { get; set; }
        public int? OrderId { get; set; }
        public string CommentsText { get; set; }
        public int Status { get; set; }
        public int Deleted { get; set; }
        public DateTime Inserted { get; set; }
        public DateTime Updated { get; set; }

        [ForeignKey("ManagerId")]
        public virtual mainManager mainManager { get; set; }
        [ForeignKey("OrderId")]
        public virtual mainOrder mainOrder { get; set; }
    }
    public class orderCommentInput
    {
        public int? Id { get; set; }
        public int ManagerId { get; set; }
        public int OrderId { get; set; }
        public string CommentsText { get; set; }
    }
}

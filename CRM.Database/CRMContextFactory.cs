﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;


namespace CRM.Database
{
    public class CRMContextFactory
    {
        public CRMContext CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var builder = new DbContextOptionsBuilder<CRMContext>();
            var connectionString = configuration.GetConnectionString("dbCRM");
            builder.UseSqlServer(connectionString);
            return new CRMContext(builder.Options);
        }
    }
}
